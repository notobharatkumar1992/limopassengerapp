package com.limocar.passenger.Async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.interfaces.AsyncGetDirection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

@SuppressWarnings("deprecation")
public class GetKMAsync extends AsyncTask<Void, ProgressDialog, String> {

    public Context mcontext;
    public AsyncGetDirection onTaskCompleted;
    public String mUrl;
    public Double sLatitude, sLongitude;
    public Double dLatitude, dLongitude;

    private ArrayList<LatLng> listLatLng;
    private String time;

    public GetKMAsync(Context con, AsyncGetDirection mAsyncGetDirection,
                      Double sLatitude, Double sLongitude, Double dLatitude,
                      Double dLongitude) {
        this.mcontext = con;
        this.onTaskCompleted = mAsyncGetDirection;
        this.sLatitude = sLatitude;
        this.sLongitude = sLongitude;
        this.dLatitude = dLatitude;
        this.dLongitude = dLongitude;
    }

    @Override
    protected String doInBackground(Void... param) {
        try {
            mUrl = "http://maps.googleapis.com/maps/api/directions/json?"
                    + "origin=" + sLatitude + "," + sLongitude
                    + "&destination=" + dLatitude + "," + dLongitude
                    + "&sensor=false&units=metric&mode=driving";

            AppDelegate.LogUA("direction url => " + mUrl);

            HttpClient mHttpClient = new DefaultHttpClient(AppDelegate.getHttpParameters());
            HttpContext mHttpContext = new BasicHttpContext();
            mUrl = mUrl.replace(" ", "%20");
            HttpGet mHttpget = new HttpGet(mUrl.trim());
            HttpResponse mHttpResponse = mHttpClient.execute(mHttpget,
                    mHttpContext);
            if (mHttpResponse != null) {
                HttpEntity mHttpEntity = mHttpResponse.getEntity();
                InputStream inStream = mHttpEntity.getContent();
                if (inStream != null) {
                    String result = AppDelegate.convertStreamToString(inStream);
                    if (result != null && result.length() > 0) {
                        return result;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        listLatLng = new ArrayList<LatLng>();
        try {
            if (result == null) {
                onTaskCompleted.OnDirectionCompleted(listLatLng, Tags.ERROR);
            } else {
                JSONObject json = new JSONObject(result);
                AppDelegate.LogUR(json.toString());
                JSONArray routeArray = json.getJSONArray("routes");
                JSONObject routes = routeArray.getJSONObject(0);
                time = routes.getJSONArray("legs").getJSONObject(0)
                        .getJSONObject("distance").getString("text");
                listLatLng = null;
                onTaskCompleted.OnDirectionCompleted(listLatLng, time);
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
            onTaskCompleted.OnDirectionCompleted(listLatLng, Tags.ERROR);
        }
    }

}
