package com.limocar.passenger.activities;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.net.Callback;
import com.limocar.passenger.net.RestError;
import com.limocar.passenger.net.SingletonRestClient;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 12-Jan-17.
 */

public class SupportActivity extends AppCompatActivity implements View.OnClickListener {

    private MaterialEditText met_comment;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.color.darkgreen);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.support_activity);
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(SupportActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SupportActivity.this);
                        break;

                }
            }
        };
    }

    private void initView() {
        met_comment = (MaterialEditText) findViewById(R.id.met_comment);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        findViewById(R.id.txt_c_call_support).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_call_support:
                AppDelegate.callNumber(this, getString(R.string.contact_01));
                break;
            case R.id.txt_c_submit:
                executeSignInAsync();
                break;

        }
    }

    private void executeSignInAsync() {
        if (met_comment.length() == 0) {
            AppDelegate.showToast(this, "Please enter username.");
        } else if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().userForgetPassword(met_comment.getText().toString(), new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                    mHandler.sendEmptyMessage(11);
                    AppDelegate.showToast(SupportActivity.this, getString(R.string.try_again));
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
//                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                    parseForgotResponse(new String(((TypedByteArray) response.getBody()).getBytes()));
                }
            });
        }
    }

    private void parseForgotResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(Tags.s) == 0) {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
