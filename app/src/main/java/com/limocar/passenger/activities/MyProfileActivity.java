package com.limocar.passenger.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.Window;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.TransitionHelper;
import com.limocar.passenger.models.UserDataModel;
import com.limocar.passenger.utils.CircleImageView;
import com.limocar.passenger.utils.Prefs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.ByteArrayOutputStream;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Bharat on 12-Jan-17.
 */

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private MaterialEditText met_old_password, met_new_password, met_confirm_password;
    private Handler mHandler;
    private CircleImageView cimg_user;
    private ImageView img_c_loading1;
    private TextView txt_c_name, txt_c_email, txt_c_phone;
    private UserDataModel userDataModel;
    private Prefs prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.my_profile_activity);
        initView();
        prefs = new Prefs(this);
        setHandler();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    private void setData() {
        userDataModel = prefs.getUserdata();
        if (userDataModel != null) {
            txt_c_name.setText(userDataModel.first_name + " " + (AppDelegate.isValidString(userDataModel.last_name) ? userDataModel.last_name : ""));
            txt_c_email.setText(userDataModel.email);
            txt_c_phone.setText(userDataModel.phone);
            AppDelegate.LogT("Pic==>" + userDataModel.image);
            try {
                String[] safe0 = userDataModel.image.split(",");
//            String[] safe = safe0[1].split("=");
                byte[] imageAsBytes = Base64.decode(safe0[1].getBytes(), Base64.DEFAULT);
                cimg_user.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }

        }
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MyProfileActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MyProfileActivity.this);
                        break;

                }
            }
        };
    }

    private void initView() {
        met_old_password = (MaterialEditText) findViewById(R.id.met_old_password);
        met_new_password = (MaterialEditText) findViewById(R.id.met_new_password);
        met_confirm_password = (MaterialEditText) findViewById(R.id.met_confirm_password);
        findViewById(R.id.rl_c_change_password).setOnClickListener(this);
        findViewById(R.id.txt_c_add_emergency_contacts).setOnClickListener(this);
        findViewById(R.id.txt_c_address_book).setOnClickListener(this);
        findViewById(R.id.img_c_edit_profile).setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);
        img_c_loading1 = (ImageView) findViewById(R.id.img_c_loading1);
        txt_c_name = (TextView) findViewById(R.id.txt_c_name);
        txt_c_email = (TextView) findViewById(R.id.txt_c_email);
        txt_c_phone = (TextView) findViewById(R.id.txt_c_phone);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_c_change_password: {

                break;
            }
            case R.id.txt_c_add_emergency_contacts: {
                Intent intent = new Intent(MyProfileActivity.this, SignUpActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(MyProfileActivity.this, false,
                        new Pair<>(findViewById(R.id.txt_c_sign_up), getString(R.string.title_name)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MyProfileActivity.this, pairs);
                startActivity(intent, transitionActivityOptions.toBundle());
                break;
            }
            case R.id.txt_c_address_book: {
                break;
            }
            case R.id.img_c_edit_profile: {
                startActivity(new Intent(MyProfileActivity.this, EditProfileActivity.class));
                break;
            }
            case R.id.img_c_back: {
                onBackPressed();
//                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
