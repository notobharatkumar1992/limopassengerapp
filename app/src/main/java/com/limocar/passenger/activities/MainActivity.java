package com.limocar.passenger.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ListView;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.fragments.HomeFragment;
import com.limocar.passenger.models.UserDataModel;
import com.limocar.passenger.net.Callback;
import com.limocar.passenger.net.RestError;
import com.limocar.passenger.net.SingletonRestClient;
import com.limocar.passenger.utils.CircleImageView;
import com.limocar.passenger.utils.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import SlidingPaneLayout.SlidingPaneLayout1;
import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 17-Jan-17.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int PANEL_HOME = 0, PANEL_MYPROFILE = 1, PANEL_PAYMENT = 2, PANEL_FAVOURITES = 3, PANEL_HISTORY = 4, PANEL_NOTIFICATIONS = 6, PANEL_SCHEDULED = 5, PANEL_SETTING = 7, PANEL_LOGOUT = 8;
    public LinearLayout ll_c_payment, ll_c_history, ll_c_favourites, ll_c_scheduled, ll_c_notifications, ll_c_settings, ll_c_logout;
    public RelativeLayout rl_c_name;
    public CircleImageView cimg_user;
    public ImageView img_c_loading;
    public TextView txt_c_name;
    TextView txt_c_payment, txt_c_history, txt_c_favourites, txt_c_scheduled, txt_c_notifications, txt_c_settings, txt_c_logout;

    public android.widget.LinearLayout side_panel;
    public SlidingPaneLayout1 mSlidingPaneLayout;
    public SlideMenuClickListener menuClickListener = new SlideMenuClickListener();
    public boolean isSlideOpen = false;
    public int ratio, ride_cancel_time = 30;
    public float init = 0.0f;
    public Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        menuClickListener = new SlideMenuClickListener();
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                if (msg.what == 0) {
                    toggleSlider();
                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
        displayView(PANEL_HOME);
    }

    private void setData() {
        UserDataModel userDataModel = new Prefs(this).getUserdata();
        if (userDataModel != null) {
            txt_c_name.setText(userDataModel.first_name + " " + (AppDelegate.isValidString(userDataModel.last_name) ? userDataModel.last_name : ""));
            AppDelegate.LogT("Pic==>" + userDataModel.image);
            try {
                String[] safe0 = userDataModel.image.split(",");
//            String[] safe = safe0[1].split("=");
                byte[] imageAsBytes = Base64.decode(safe0[1].getBytes(), Base64.DEFAULT);
                cimg_user.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }

        }
    }

    private void initView() {
        mSlidingPaneLayout = (SlidingPaneLayout1) findViewById(R.id.mSlidingPaneLayout);
        mSlidingPaneLayout.setSliderFadeColor(getResources().getColor(android.R.color.transparent));
        mSlidingPaneLayout.setPanelSlideListener(new SliderListener());
        side_panel = (android.widget.LinearLayout) findViewById(R.id.side_panel);

        findViewById(R.id.img_c_menu).setOnClickListener(this);
        findViewById(R.id.rl_c_name).setOnClickListener(this);

        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);
        cimg_user.setOnClickListener(this);
        txt_c_name = (TextView) findViewById(R.id.txt_c_name);
        txt_c_payment = (TextView) findViewById(R.id.txt_c_payment);
        txt_c_history = (TextView) findViewById(R.id.txt_c_history);
        txt_c_favourites = (TextView) findViewById(R.id.txt_c_favourites);
        txt_c_scheduled = (TextView) findViewById(R.id.txt_c_scheduled);
        txt_c_notifications = (TextView) findViewById(R.id.txt_c_notifications);
        txt_c_settings = (TextView) findViewById(R.id.txt_c_settings);
        txt_c_logout = (TextView) findViewById(R.id.txt_c_logout);
        findViewById(R.id.img_c_menu).setOnClickListener(this);
        findViewById(R.id.ll_c_payment).setOnClickListener(this);
        findViewById(R.id.ll_c_history).setOnClickListener(this);
        findViewById(R.id.ll_c_favourites).setOnClickListener(this);
        findViewById(R.id.ll_c_scheduled).setOnClickListener(this);
        findViewById(R.id.ll_c_notifications).setOnClickListener(this);
        findViewById(R.id.ll_c_settings).setOnClickListener(this);
        findViewById(R.id.ll_c_logout).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_menu:
                mHandler.sendEmptyMessage(0);
                break;
            case R.id.rl_c_name:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_MYPROFILE, PANEL_MYPROFILE);
                break;
            case R.id.cimg_user:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_MYPROFILE, PANEL_MYPROFILE);
                break;
            case R.id.ll_c_logout:
                mHandler.sendEmptyMessage(0);
                executeLogoutApi();
                break;
        }
    }

    private void executeLogoutApi() {
        if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().logout(AppDelegate.session,new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.showToast(MainActivity.this, obj_json.getString(Tags.m));
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.s).contains("0")) {
                            AppDelegate.showToast(MainActivity.this, obj_json.getString(Tags.m));
                            new Prefs(MainActivity.this).clearSharedPreference();
                            startActivity(new Intent(MainActivity.this, SignInActivity.class));
                            AppDelegate.session = null;
                            finish();
                        } else {
                            AppDelegate.showAlert(MainActivity.this, obj_json.getString(Tags.extra));
                        }
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    class SliderListener extends SlidingPaneLayout1.SimplePanelSlideListener {
        @Override
        public void onPanelOpened(View panel) {
            if (!isSlideOpen) {
                isSlideOpen = true;
            }
            AppDelegate.hideKeyBoard(MainActivity.this);
        }

        @Override
        public void onPanelClosed(View panel) {
            if (isSlideOpen) {
                isSlideOpen = false;
            }
        }

        @Override
        public void onPanelSlide(View view, float slideOffset) {
            ratio = (int) -(0 - (slideOffset) * 255);
            whileSlide(ratio);
        }
    }

    public void toggleSlider() {
        if (mSlidingPaneLayout != null)
            if (!mSlidingPaneLayout.isOpen()) {
                mSlidingPaneLayout.openPane();
            } else {
                mSlidingPaneLayout.closePane();
            }
    }

    public void whileSlide(int view) {
        int newalfa = 255 - view;
        float subvalue = newalfa / 2.55f;
        float f = (100f - subvalue) * 0.01f;
        Animation alphaAnimation = new AlphaAnimation(init, f);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        init = f;
        side_panel.startAnimation(alphaAnimation);
    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayView(position);
                }
            }, 600);
        }
    }

    public void setInitailSideBar(int value) {
        txt_c_payment.setSelected(false);
        txt_c_history.setSelected(false);
        txt_c_favourites.setSelected(false);
        txt_c_scheduled.setSelected(false);
        txt_c_notifications.setSelected(false);
        txt_c_settings.setSelected(false);
        txt_c_logout.setSelected(false);
        switch (value) {
            case PANEL_HOME:
                break;
            case PANEL_MYPROFILE:
                break;
            case PANEL_PAYMENT:
                txt_c_payment.setSelected(true);
                break;
            case PANEL_FAVOURITES:
                txt_c_favourites.setSelected(true);
                break;
            case PANEL_SETTING:
                txt_c_settings.setSelected(true);
                break;
            case PANEL_LOGOUT:
                txt_c_logout.setSelected(true);
                break;
            case PANEL_HISTORY:
                txt_c_history.setSelected(true);
                break;
            case PANEL_SCHEDULED:
                txt_c_scheduled.setSelected(true);
                break;
            case PANEL_NOTIFICATIONS:
                txt_c_notifications.setSelected(true);
                break;
        }
    }
    @Override
    public void onBackPressed() {
        if (mSlidingPaneLayout.isOpen())
            mSlidingPaneLayout.closePane();
        else if (getSupportFragmentManager().findFragmentById(R.id.main_containt) instanceof HomeFragment)
            finish();
        else super.onBackPressed();
    }

    private void displayView(int position) {
        AppDelegate.hideKeyBoard(MainActivity.this);
        setInitailSideBar(position);
        switch (position) {
            case PANEL_HOME:
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new HomeFragment(), R.id.main_containt);
                return;
            case PANEL_MYPROFILE:
                startActivity(new Intent(this, MyProfileActivity.class));
                break;
            case PANEL_PAYMENT:
                break;
            case PANEL_FAVOURITES:
                break;
            case PANEL_HISTORY:
                break;
            case PANEL_SCHEDULED:
//                AppDelegate.showFragmentAnimation(getSupportFragmentManager(),fragment, R.id.main_containt);
                break;
            case PANEL_NOTIFICATIONS:
                break;
            case PANEL_SETTING:
                break;
            case PANEL_LOGOUT:
                startActivity(new Intent(MainActivity.this, SignInActivity.class));
                finish();
                break;

        }
    }

}
