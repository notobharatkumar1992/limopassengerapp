package com.limocar.passenger.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.TransitionHelper;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.models.UserDataModel;
import com.limocar.passenger.net.Callback;
import com.limocar.passenger.net.RestError;
import com.limocar.passenger.net.SingletonRestClient;
import com.limocar.passenger.utils.Prefs;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 12-Jan-17.
 */

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private MaterialEditText met_username, met_password;
    private Handler mHandler;
    public static Activity mActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.signin_activity_01);
        AppDelegate.session = "";
        mActivity = this;
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(SignInActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SignInActivity.this);
                        break;

                }
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    private void initView() {
        met_username = (MaterialEditText) findViewById(R.id.met_username);
        met_password = (MaterialEditText) findViewById(R.id.met_password);
        findViewById(R.id.txt_c_forgot_password).setOnClickListener(this);
        findViewById(R.id.txt_c_sign_up).setOnClickListener(this);
        findViewById(R.id.txt_c_sign_in).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_forgot_password: {
                Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SignInActivity.this, false,
                        new Pair<>(findViewById(R.id.img_logo), getString(R.string.img_logo)),
                        new Pair<>(findViewById(R.id.img_right_top), getString(R.string.img_right_top)),
                        new Pair<>(findViewById(R.id.txt_c_forgot_password), getString(R.string.title_name)),
                        new Pair<>(met_username, getString(R.string.met_username)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SignInActivity.this, pairs);
                startActivity(intent, transitionActivityOptions.toBundle());
                break;
            }
            case R.id.txt_c_sign_up: {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SignInActivity.this, false,
                        new Pair<>(findViewById(R.id.txt_c_sign_up), getString(R.string.title_name)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SignInActivity.this, pairs);
                startActivity(intent, transitionActivityOptions.toBundle());
                break;
            }
            case R.id.txt_c_sign_in: {
                executeSignInAsync();
//                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void executeSignInAsync() {
        if (met_username.length() == 0) {
            AppDelegate.showToast(this, "Please enter username.");
        } else if (!AppDelegate.CheckEmail(met_username.getText().toString())) {
            AppDelegate.showToast(this, "Please enter valid username.");
        } else if (met_password.length() == 0) {
            AppDelegate.showToast(this, "Please enter password.");
        } else if (AppDelegate.haveNetworkConnection(this)) {

            String device_token = new Prefs(SignInActivity.this).getGCMtokenfromTemp();
            if (!AppDelegate.isValidString(device_token)) {
                device_token = "Not found";
            }
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().userLogin(met_username.getText().toString(), met_password.getText().toString(), AppDelegate.getUUID(SignInActivity.this), /*"A",*/ device_token, new Callback<Response>() {

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (AppDelegate.isValidString(obj_json.getString(Tags.d))) {
                            AppDelegate.showToast(SignInActivity.this, obj_json.getString(Tags.m));
                            JSONObject object = obj_json.getJSONObject(Tags.d);
                            UserDataModel userDataModel = new UserDataModel();
                            userDataModel.first_name = object.getString(Tags.first_name);
                            userDataModel.last_name = object.getString(Tags.last_name);
                            userDataModel.email = object.getString(Tags.username);
                            userDataModel.phone = object.getString(Tags.phone);
                            userDataModel.image = object.getString(Tags.pic);
                            userDataModel.session = object.getString(Tags.session);
                            userDataModel.k1 = object.getString(Tags.k1);
                            userDataModel.k2 = object.getString(Tags.k2);
                            AppDelegate.session = userDataModel.session;
                            new Prefs(SignInActivity.this).setUserData(userDataModel);
                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                            finish();
                        } else {
                            AppDelegate.checkJsonMessage(SignInActivity.this, obj_json);

                        }
//                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void failure(RestError restError) {
//                    mHandler.sendEmptyMessage(11);
//                    AppDelegate.LogT("restError = " + restError.getcode() + ", " + restError.getstrMessage());
//                    AppDelegate.showToast(SignInActivity.this, restError.getstrMessage());
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
//                        AppDelegate.LogT("restError = " + new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())));
                        AppDelegate.checkJsonMessage(SignInActivity.this, obj_json);

                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }
}
