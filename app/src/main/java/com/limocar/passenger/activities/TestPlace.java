package com.limocar.passenger.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;


import static com.limocar.passenger.R.styleable.TextView;

public class TestPlace extends AppCompatActivity {
android.widget.TextView et_pickup,et_dropoff;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_place);
        et_dropoff=(TextView)findViewById(R.id.et_dropoff);
        et_pickup=(TextView)findViewById(R.id.et_pickup);
      /*  PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
*/
  /*      autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                AppDelegate.LogT("Place response ==>" + place);
                LatLng latLng = (place.getLatLng());
                double lat = latLng.latitude;
                AppDelegate.LogT("Place lat lng ==>" + place.getLatLng() + ", latitude==>" + place.getLatLng().latitude + ", longitude==>" + place.getLatLng().longitude);
                AppDelegate.LogT("Place: " + place.getName());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                AppDelegate.LogT("An error occurred: " + status);
            }
        });*/
        findViewById(R.id.et_pickup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .build(TestPlace.this);
                    startActivityForResult(intent, 200);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });
        findViewById(R.id.et_dropoff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .build(TestPlace.this);
                    startActivityForResult(intent, 300);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 200) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                AppDelegate.LogT("Place pick up response ==>" + place);
                LatLng latLng = (place.getLatLng());
                double lat = latLng.latitude;
                AppDelegate.LogT("Place lat lng ==>" + place.getLatLng() + ", latitude==>" + place.getLatLng().latitude + ", longitude==>" + place.getLatLng().longitude);
                AppDelegate.LogT("Place: " + place.getName());
et_pickup.setText(place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                AppDelegate.LogT(status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == 300) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                AppDelegate.LogT("Place drop offresponse ==>" + place);
                LatLng latLng = (place.getLatLng());
                double lat = latLng.latitude;
                AppDelegate.LogT("Place lat lng ==>" + place.getLatLng() + ", latitude==>" + place.getLatLng().latitude + ", longitude==>" + place.getLatLng().longitude);
                AppDelegate.LogT("Place: " + place.getName());
                et_dropoff.setText(place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                AppDelegate.LogT(status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
