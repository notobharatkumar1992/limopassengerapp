package com.limocar.passenger.activities;

import android.content.IntentSender;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.Async.LocationAddress;
import com.limocar.passenger.Async.PlacesService;
import com.limocar.passenger.R;
import com.limocar.passenger.adapters.LocationMapAdapter;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.expandablelayout.ExpandableRelativeLayout;
import com.limocar.passenger.interfaces.OnListItemClickListener;
import com.limocar.passenger.models.BookOrderModel;
import com.limocar.passenger.models.Place;
import com.limocar.passenger.utils.Prefs;
import com.limocar.passenger.utils.WorkaroundMapFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ImageView;
import carbon.widget.ProgressBar;
import carbon.widget.TextView;

import static com.limocar.passenger.fragments.HomeFragment.PICK_UP;

/**
 * Created by HEENA on 07/29/2016.
 */
public class MapActivity extends AppCompatActivity implements OnListItemClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, WorkaroundMapFragment.OnTouchListener, View.OnClickListener {

    private GoogleMap map_business;
    private String name = "", country = "", state = "", city = "", image = "", company_name = "";
    private Handler mHandler;
    private TextView txt_c_title, txt_c_save;
    ImageView img_c_loading1;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;
    WorkaroundMapFragment map;
    TextView txt_c_address;
    int type = PICK_UP;
    public List<Place> mapPlacesArray_pickup = new ArrayList<>();
    public Thread mThreadOnSearch_pickup;
    public LocationMapAdapter adapter_pickup;
    public ListView search_list_pickup;
    public TextView no_location_text_pickup;
    public ProgressBar pb_c_location_progressbar_pickup;

    public Place placeDetail_pickup;
    public ExpandableRelativeLayout exp_layout;
    private EditText et_pickup;
    public static final int COLLAPSE = 0, EXPAND = 1;
    BookOrderModel bookOrderModel;
    Prefs prefs;

    public boolean canSearch = false;
    public boolean fetchFromMapLocation = false;
    public boolean fromCurrentLocation = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.color.colorPrimary);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.map_activity);
        type = getIntent().getIntExtra(Tags.type, PICK_UP);
        prefs = new Prefs(this);
        if (prefs.getbookOrderData() != null) {
            bookOrderModel = prefs.getbookOrderData();
        } else
            bookOrderModel = new BookOrderModel();
        initGPS();
        showGPSalert();
        setHandler();
        initView();
    }

    private void showGPSalert() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(MapActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    MapActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    private void initGPS() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MapActivity.this);
                mGoogleApiClient.disconnect();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView() {
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        txt_c_save = (TextView) findViewById(R.id.txt_c_save);
        txt_c_save.setVisibility(View.GONE);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);
        img_c_loading1 = (ImageView) findViewById(R.id.img_c_loading1);
        findViewById(R.id.img_c_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        map = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map);
        map.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map_business = googleMap;
                showMap();
            }
        });
        findViewById(R.id.img_c_back).setOnClickListener(this);
        et_pickup = (EditText) findViewById(R.id.et_pickup);
        et_pickup.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        if (type == PICK_UP) {
            ((android.widget.ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.marker_green);
            ((android.widget.ImageView) findViewById(R.id.img_green_dot)).setImageResource(R.drawable.green_dot);
            et_pickup.setHint("Choose pick up location");
            txt_c_save.setText("S E T  A S  P I C K - U P");
        } else {
            ((android.widget.ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.marker_orange);
            ((android.widget.ImageView) findViewById(R.id.img_green_dot)).setImageResource(R.drawable.red_dot);
            et_pickup.setHint("Choose drop off location");
            txt_c_save.setText("S E T  A S  D R O P - O F F");
        }
        addtextWatcher();
        no_location_text_pickup = (TextView) findViewById(R.id.no_location_text);
        search_list_pickup = (ListView) findViewById(R.id.search_list);
        adapter_pickup = new LocationMapAdapter(MapActivity.this, mapPlacesArray_pickup, this);
        search_list_pickup.setAdapter(adapter_pickup);
        pb_c_location_progressbar_pickup = (ProgressBar) findViewById(R.id.pb_c_location_progressbar);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        expandableView(exp_layout, COLLAPSE);
        findViewById(R.id.img_c_back).setOnClickListener(this);
//        findViewById(R.id.img_c_map).setOnClickListener(this);
        findViewById(R.id.txt_c_save).setOnClickListener(this);

    }

    private void addtextWatcher() {
        et_pickup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mHandler.sendEmptyMessage(5);
            }
        });
    }

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    private String place_name = "", place_address = "";

    private void showMap() {
        if (map_business == null) {
            return;
        }
        map.setListener(this);
        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(true);
        map_business.getUiSettings().setCompassEnabled(true);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        try {
            View locationButton = ((View) map.getView().findViewById(0x2).getParent()).findViewById(0x1);
            android.widget.RelativeLayout.LayoutParams rlp = (android.widget.RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            rlp.addRule(android.widget.RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
            rlp.setMargins(30, 30, 20, 20);
            rlp.addRule(android.widget.RelativeLayout.ALIGN_PARENT_START, android.widget.RelativeLayout.TRUE);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        map_business.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if (fetchFromMapLocation) {
                    AppDelegate.LogT("centerLat => " + cameraPosition.target.latitude + ", " + cameraPosition.target.longitude + "");
                    currentLatitude = cameraPosition.target.latitude;
                    currentLongitude = cameraPosition.target.longitude;
                    canSearch = false;
                    mHandler.sendEmptyMessage(10);
                    LocationAddress.getAddressFromLocation(currentLatitude, currentLongitude, MapActivity.this, mHandler);
                }
            }
        });
        if (type == PICK_UP) {
            if (bookOrderModel != null && bookOrderModel.picup_latitude != 0 && bookOrderModel.pickup_longitude != 0) {
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(prefs.getbookOrderData().picup_latitude, prefs.getbookOrderData().pickup_longitude))
                        .zoom(15.0f).build();
                map_business.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                et_pickup.setText(bookOrderModel.pickup_address);
                currentLatitude = bookOrderModel.picup_latitude;
                currentLongitude = bookOrderModel.pickup_longitude;
                txt_c_save.setVisibility(View.VISIBLE);
                canSearch = true;
            } else {
                fromCurrentLocation = true;
                txt_c_save.setVisibility(View.GONE);
            }
        } else {
            if (bookOrderModel != null && bookOrderModel.dropoff_latitude != 0 && bookOrderModel.dropoff_longitude != 0) {
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(prefs.getbookOrderData().dropoff_latitude, prefs.getbookOrderData().dropoff_longitude))
                        .zoom(15.0f).build();
                map_business.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                et_pickup.setText(bookOrderModel.dropoff_address);
                currentLatitude = bookOrderModel.dropoff_latitude;
                currentLongitude = bookOrderModel.dropoff_longitude;
                txt_c_save.setVisibility(View.VISIBLE);
                canSearch = true;
            } else {
                fromCurrentLocation = true;
                txt_c_save.setVisibility(View.GONE);
            }
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetchFromMapLocation = true;
            }
        }, 2000);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MapActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MapActivity.this);
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        break;
                    case 4:
                        AppDelegate.LogT("Handler 4 called");
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        if (adapter_pickup != null && search_list_pickup != null) {
                            adapter_pickup.notifyDataSetChanged();
                            search_list_pickup.invalidate();
                        }
                        if (mapPlacesArray_pickup != null && mapPlacesArray_pickup.size() > 0) {
                            if (et_pickup.length() > 0) {
                                AppDelegate.LogT("handler 1");
                                search_list_pickup.setVisibility(View.VISIBLE);
                                no_location_text_pickup.setVisibility(View.GONE);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            search_list_pickup.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult) {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            } else {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text_pickup.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 5:
                        AppDelegate.LogT("Handler 5 called => " + canSearch);
                        if (canSearch) {
                            if (et_pickup.length() != 0) {
                                pb_c_location_progressbar_pickup.setVisibility(View.VISIBLE);
                                stopStartThread(mThreadOnSearch_pickup);
                                no_location_text_pickup.setVisibility(View.GONE);
                                expandableView(exp_layout, EXPAND);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                            txt_c_save.setVisibility(View.GONE);
                        } else {
                            expandableView(exp_layout, COLLAPSE);
                        }
                        break;
                    case 9:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 21:
                        no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        no_location_text_pickup.setVisibility(View.VISIBLE);
                        search_list_pickup.setVisibility(View.GONE);
                        expandableView(exp_layout, EXPAND);
                        break;
                    case 22:
                        if (et_pickup.length() != 0) {
                        } else {
                            expandableView(exp_layout, COLLAPSE);
                        }
                        break;
                }
            }
        };
    }

    private void getLatLngFromGEOcoder(Bundle data) {
        mHandler.sendEmptyMessage(11);
        AppDelegate.LogT("for latlng => " + data.getString(Tags.forLatlng) + ", latitude = " + currentLatitude + ", longitude = " + currentLongitude);
        if (AppDelegate.isValidString(data.getString(Tags.forLatlng)) && data.getString(Tags.forLatlng).equalsIgnoreCase(Tags.forLatlng)) {
            try {
                currentLatitude = data.getDouble(Tags.latitude);
                currentLongitude = data.getDouble(Tags.longitude);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            txt_c_save.setVisibility(View.VISIBLE);
            fetchFromMapLocation = false;
            int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (status == ConnectionResult.SUCCESS) {
                try {
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatitude, currentLongitude)).zoom(15.0f).build();
                    map_business.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                GooglePlayServicesUtil.getErrorDialog(status, this, status);
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AppDelegate.LogT("After 1 sec => fetchFromMapLocation = true ");
                    fetchFromMapLocation = true;
                }
            }, 2000);
        } else {
            txt_c_save.setVisibility(View.GONE);
        }

    }

    private void stopStartThread(Thread theThread) {
        stopTimer();
        onlyStopThread(mThreadOnSearch_pickup);
        if (!AppDelegate.haveNetworkConnection(MapActivity.this, false)) {
            no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
            no_location_text_pickup.setVisibility(View.VISIBLE);
            search_list_pickup.setVisibility(View.GONE);
            expandableView(exp_layout, EXPAND);
        } else {
            expandableView(exp_layout, EXPAND);
            mTimer = new Timer();
            mtimerTask = new TimerTask() {
                public void run() {
                    mThreadOnSearch_pickup = new Thread(mTimerRunnable);
                    mThreadOnSearch_pickup.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null) {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (!AppDelegate.haveNetworkConnection(MapActivity.this, false)) {
                mHandler.sendEmptyMessage(21);
            } else if (et_pickup.length() == 0) {
                mHandler.sendEmptyMessage(22);
            } else {
                mapPlacesArray_pickup.clear();
                mapPlacesArray_pickup.addAll(PlacesService.autocompleteForMap(et_pickup.getText().toString()));
                mHandler.sendEmptyMessage(4);
                canSearch = true;
            }
        }
    };

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    private void setAddressFromGEOcoder(Bundle data) {
        AppDelegate.LogT("setAddressFromGEOcoder called => " + data.toString());
        canSearch = false;
        mHandler.sendEmptyMessage(11);
        if (AppDelegate.isValidString(data.getString(Tags.ADDRESS))) {
            currentLatitude = data.getDouble(Tags.LAT);
            currentLongitude = data.getDouble(Tags.LNG);
            canSearch = false;
            et_pickup.setText(data.getString(Tags.ADDRESS));
            fetchFromMapLocation = false;
            int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (status == ConnectionResult.SUCCESS) {
                try {
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatitude, currentLongitude)).zoom(15.0f).build();
                    map_business.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                GooglePlayServicesUtil.getErrorDialog(status, this, status);
            }
            txt_c_save.setVisibility(View.VISIBLE);
        } else {
            txt_c_save.setVisibility(View.GONE);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AppDelegate.LogT("After 1 sec => fetchFromMapLocation = true ");
                fetchFromMapLocation = true;
                canSearch = true;
            }
        }, 2000);

    }


    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, MapActivity.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            new Prefs(this).putStringValue(Tags.LAT, String.valueOf(location.getLatitude()));
            new Prefs(this).putStringValue(Tags.LNG, String.valueOf(location.getLongitude()));
            if (fromCurrentLocation) {
                canSearch = false;
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();
                mHandler.sendEmptyMessage(10);
                LocationAddress.getAddressFromLocation(currentLatitude, currentLongitude, this, mHandler);
                AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
        mGoogleApiClient.connect();
//        if (prefs.getbookOrderData() != null) {
//            bookOrderModel = prefs.getbookOrderData();
//            if (type == PICK_UP)
//                et_pickup.setText(bookOrderModel.pickup_address);
//            else
//                et_pickup.setText(bookOrderModel.dropoff_address);
//        } else
//            bookOrderModel = new BookOrderModel();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        AppDelegate.LogGP("onLocationChanged latLng = " + currentLatitude + ", " + currentLongitude);
        new Prefs(this).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
        new Prefs(this).putStringValue(Tags.LNG, String.valueOf(currentLongitude));
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MapActivity.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onTouch() {
        AppDelegate.LogT("on touch called");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                finish();
                break;
            case R.id.txt_c_save:
                saveAddress();
                break;
        }
    }

    private void saveAddress() {
        if (type == PICK_UP) {
            bookOrderModel.pickup_address = et_pickup.getText().toString();
            bookOrderModel.picup_latitude = currentLatitude;
            bookOrderModel.pickup_longitude = currentLongitude;
            prefs.setBookOrderData(bookOrderModel);
        } else {
            bookOrderModel.dropoff_address = et_pickup.getText().toString();
            bookOrderModel.dropoff_latitude = currentLatitude;
            bookOrderModel.dropoff_longitude = currentLongitude;
            prefs.setBookOrderData(bookOrderModel);
        }
        if (ChooseLocationActivity.mActivity != null)
            ChooseLocationActivity.mActivity.finish();
        finish();
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.hideKeyBoard(MapActivity.this);
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(MapActivity.this)) {
            canSearch = false;
            if (mapPlacesArray_pickup.size() > position) {
                placeDetail_pickup = mapPlacesArray_pickup.get(position);
                if (AppDelegate.isValidString(placeDetail_pickup.name) && AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_pickup.setText(placeDetail_pickup.name + ", " + placeDetail_pickup.vicinity);
                } else if (AppDelegate.isValidString(placeDetail_pickup.name)) {
                    et_pickup.setText(placeDetail_pickup.name);
                } else if (AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_pickup.setText(placeDetail_pickup.vicinity);
                }
                et_pickup.setSelection(et_pickup.length());
                expandableView(exp_layout, COLLAPSE);
                mHandler.sendEmptyMessage(10);
                LocationAddress.getLocationFromReverseGeocoder(et_pickup.getText().toString(), MapActivity.this, mHandler);
            }
            canSearch = true;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {
    }
}
