package com.limocar.passenger.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.models.UserDataModel;
import com.limocar.passenger.net.Callback;
import com.limocar.passenger.net.RestError;
import com.limocar.passenger.net.SingletonRestClient;
import com.limocar.passenger.utils.CircleImageView;
import com.limocar.passenger.utils.Prefs;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 12-Jan-17.
 */
public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private CircleImageView cimg_user;
    private MaterialEditText met_first_name, met_last_name, met_contact_no, met_username, met_password, met_confirm_password;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sign_up);
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(SignUpActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SignUpActivity.this);
                        break;

                }
            }
        };
    }

    private void initView() {
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);
        cimg_user.setOnClickListener(this);
        met_first_name = (MaterialEditText) findViewById(R.id.met_first_name);
        met_last_name = (MaterialEditText) findViewById(R.id.met_last_name);
        met_contact_no = (MaterialEditText) findViewById(R.id.met_contact_no);
        met_username = (MaterialEditText) findViewById(R.id.met_username);
        met_password = (MaterialEditText) findViewById(R.id.met_password);
        met_confirm_password = (MaterialEditText) findViewById(R.id.met_confirm_password);
        findViewById(R.id.rl_user_img).setVisibility(View.GONE);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_submit:
                executeSignInAsync();
                break;
            case R.id.cimg_user:
                showImageSelectorList();
                break;
        }
    }

    /* image selection*/
    public static File capturedFile;
    public static Uri imageURI = null;

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(SignUpActivity.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        ListView modeList = new ListView(SignUpActivity.this);
        String[] stringArray = new String[]{"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(SignUpActivity.this, R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                Bitmap OriginalPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), picUri);
                OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                cimg_user.setImageBitmap(OriginalPhoto);
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile(SignUpActivity.this);
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(SignUpActivity.this, getString(R.string.file_not_created));
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    public static String getNewFile(Context mContext) {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + mContext.getString(R.string.app_name));
        } else {
            directoryFile = mContext.getDir(mContext.getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), AppDelegate.SELECT_PICTURE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult MainActivity");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppDelegate.SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(SignUpActivity.this, data.getData());
                } else {
                    Toast.makeText(SignUpActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            } else if (requestCode == AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE) {
                if (imageURI != null) {
                    startCropActivity(SignUpActivity.this, imageURI);
                } else {
                    Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            setOnReceivePictureResult(Tags.PICTURE, resultUri);
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    public static void startCropActivity(FragmentActivity mActivity, Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".png";
        capturedFile = new File(mActivity.getCacheDir(), destinationFileName);
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(capturedFile));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(mActivity);
    }

    public static UCrop basisConfig(@NonNull UCrop uCrop) {
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setCompressionQuality(00);

//        if (SellItemFragment.onPictureResult != null) {
//            options.setHideBottomControls(false);
//            options.setFreeStyleCropEnabled(false);
//        } else {
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);
//        }


        return uCrop.withOptions(options);
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public void writeImageFile(Bitmap bitmap, String filePath) {
        if (AppDelegate.isValidString(filePath)) {
            capturedFile = new File(filePath);
        } else {
            return;
        }
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    private void executeSignInAsync() {
       /* if (capturedFile == null) {
            AppDelegate.showToast(this, "Please select an image.");
        } else*/
        if (met_first_name.length() == 0) {
            AppDelegate.showToast(this, "Please enter First Name.");
        } else if (met_last_name.length() == 0) {
            AppDelegate.showToast(this, "Please enter Last Name.");
        } else if (met_username.length() == 0) {
            AppDelegate.showToast(this, "Please enter Email id.");
        } else if (!AppDelegate.CheckEmail(met_username.getText().toString())) {
            AppDelegate.showToast(this, "Please enter valid Email Id.");
        } else if (met_contact_no.length() == 0) {
            AppDelegate.showToast(this, "Please enter Phone Number.");
        } else if (!met_contact_no.getText().toString().contains("+")) {
            AppDelegate.showToast(this, "Invalid phone number ! Please send a phone number in international format. [+][country code][phone number including area code]");
        } else if (met_password.length() == 0) {
            AppDelegate.showToast(this, "Please enter Password.");
        } else if (met_confirm_password.length() == 0) {
            AppDelegate.showToast(this, "Please enter Confirm Password.");
        } else if (!met_password.getText().toString().equals(met_confirm_password.getText().toString())) {
            AppDelegate.showToast(this, "Please enter valid Confirm Password, must be same as Password.");
        } else if (AppDelegate.haveNetworkConnection(this)) {

            String device_token = new Prefs(SignUpActivity.this).getGCMtokenfromTemp();
            if (!AppDelegate.isValidString(device_token)) {
                device_token = "Not found";
            }
            String encodedImage = "";
            if (capturedFile != null) {
                Bitmap bm = BitmapFactory.decodeFile(capturedFile.getAbsolutePath());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                byte[] byteArrayImage = baos.toByteArray();
                encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            }
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().userRegistration(met_username.getText().toString(), met_password.getText().toString(), AppDelegate.getUUID(SignUpActivity.this), device_token, met_first_name.getText().toString(), met_last_name.getText().toString(), met_contact_no.getText().toString(), encodedImage, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(SignUpActivity.this, obj_json);
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(SignUpActivity.this, "");
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.s).contains("0")) {
                            AppDelegate.showToast(SignUpActivity.this, obj_json.getString(Tags.m));
                            JSONObject object = obj_json.getJSONObject(Tags.d);
                            UserDataModel userDataModel = new UserDataModel();
                            userDataModel.first_name = object.getString(Tags.first_name);
                            userDataModel.last_name = object.getString(Tags.last_name);
                            userDataModel.email = object.getString(Tags.username);
                            userDataModel.phone = object.getString(Tags.phone);
                            userDataModel.image = object.getString(Tags.pic);
                            userDataModel.session = object.getString(Tags.session);
                            userDataModel.k1 = object.getString(Tags.k1);
                            userDataModel.k2 = object.getString(Tags.k2);
                            new Prefs(SignUpActivity.this).setUserData(userDataModel);
                            AppDelegate.session = userDataModel.session;
                            startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                            if (SignInActivity.mActivity != null) {
                                SignInActivity.mActivity.finish();
                            }
                            finish();
                        } else {
                            AppDelegate.checkJsonMessage(SignUpActivity.this, obj_json);

                        }
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }
}
