package com.limocar.passenger.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.Async.LocationAddress;
import com.limocar.passenger.Async.PlacesService;
import com.limocar.passenger.R;
import com.limocar.passenger.adapters.LocationMapAdapter;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.expandablelayout.ExpandableRelativeLayout;
import com.limocar.passenger.fragments.AttractionListFragment;
import com.limocar.passenger.fragments.FavoriteListFragment;
import com.limocar.passenger.fragments.HotelsListFragment;
import com.limocar.passenger.fragments.TransportationListFragment;
import com.limocar.passenger.interfaces.OnListItemClickListener;
import com.limocar.passenger.models.BookOrderModel;
import com.limocar.passenger.models.Place;
import com.limocar.passenger.utils.Prefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ImageView;
import carbon.widget.ProgressBar;
import carbon.widget.TextView;

import static com.limocar.passenger.fragments.HomeFragment.DROP_OFF;
import static com.limocar.passenger.fragments.HomeFragment.ICON_PAYMENT;
import static com.limocar.passenger.fragments.HomeFragment.ICON_PROMOS;
import static com.limocar.passenger.fragments.HomeFragment.ICON_TIME;
import static com.limocar.passenger.fragments.HomeFragment.ICON_TRIP;
import static com.limocar.passenger.fragments.HomeFragment.PICK_UP;

/**
 * Created by Heena on 10-Oct-16.
 */
public class ChooseLocationActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener {

    public static Activity mActivity;
    private Handler mHandler;
    public static FragmentManager fragmentManager;
    private ImageView img_c_triptype, img_c_paymenttype, img_c_timetype, img_c_promotype;
    private EditText et_pickup, et_dropoff;
    double latitude = 0, longitude = 0;
    RelativeLayout rl_c_triptype, rl_c_paymenttype, rl_c_timetype, rl_c_promotype;
    int type = PICK_UP;
    public List<Place> mapPlacesArray_pickup = new ArrayList<>();
    public Thread mThreadOnSearch_pickup;
    public LocationMapAdapter adapter_pickup;
    public ListView search_list_pickup;
    public TextView no_location_text_pickup;
    public ProgressBar pb_c_location_progressbar_pickup;

    public Place placeDetail_pickup;
    public ExpandableRelativeLayout exp_layout;
    public static final int COLLAPSE = 0, EXPAND = 1;
    BookOrderModel bookOrderModel;
    Prefs prefs;
    boolean shouldRun = false;
    private boolean canSearch = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        setContentView(R.layout.set_location);
        type = getIntent().getIntExtra(Tags.type, PICK_UP);
        prefs = new Prefs(this);
        if (prefs.getbookOrderData() != null) {
            bookOrderModel = prefs.getbookOrderData();
        } else
            bookOrderModel = new BookOrderModel();
        setHandler();
        initView();
        setValues();
    }

    private void setValues() {
        if (prefs.getbookOrderData() != null) {
            bookOrderModel = prefs.getbookOrderData();
            if (type == PICK_UP)
                et_pickup.setText(bookOrderModel.pickup_address);
            else
                et_pickup.setText(bookOrderModel.dropoff_address);
        } else
            bookOrderModel = new BookOrderModel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ChooseLocationActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ChooseLocationActivity.this);
                        break;
                    case 1:
                        break;
                    case 2:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        break;
                    case 4:
                        AppDelegate.LogT("Handler 4 called");
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        if (adapter_pickup != null && search_list_pickup != null) {
                            adapter_pickup.notifyDataSetChanged();
                            search_list_pickup.invalidate();
                        }
                        if (mapPlacesArray_pickup != null && mapPlacesArray_pickup.size() > 0) {
                            if (et_pickup.length() > 0) {
                                AppDelegate.LogT("handler 1");
                                search_list_pickup.setVisibility(View.VISIBLE);
                                no_location_text_pickup.setVisibility(View.GONE);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            search_list_pickup.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult) {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            } else {
                                search_list_pickup.setVisibility(View.GONE);
                                no_location_text_pickup.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text_pickup.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 5:
                        AppDelegate.LogT("Handler 5 called");
                        if (canSearch) {
                            if (et_pickup.length() != 0) {
                                pb_c_location_progressbar_pickup.setVisibility(View.VISIBLE);
                                stopStartThread(mThreadOnSearch_pickup);
                                no_location_text_pickup.setVisibility(View.GONE);
                                expandableView(exp_layout, EXPAND);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            if (et_pickup.length() != 0) {
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        break;
                    case 9:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 21:
                        no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
                        pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
                        no_location_text_pickup.setVisibility(View.VISIBLE);
                        search_list_pickup.setVisibility(View.GONE);
                        expandableView(exp_layout, EXPAND);
                        break;
                    case 22:
                        if (et_pickup.length() != 0) {
                        } else {
                            expandableView(exp_layout, COLLAPSE);
                        }
                        break;
                }
            }
        };
    }

    private void getLatLngFromGEOcoder(Bundle data) {
        mHandler.sendEmptyMessage(11);
        AppDelegate.LogT("for latlng => " + data.getString(Tags.forLatlng) + ", latitude =" + latitude + ", longitude = " + longitude);
        if (AppDelegate.isValidString(data.getString(Tags.forLatlng)) && data.getString(Tags.forLatlng).equalsIgnoreCase(Tags.forLatlng)) {
            try {
                latitude = data.getDouble(Tags.latitude);
                longitude = data.getDouble(Tags.longitude);
                if (type == PICK_UP) {
                    if (prefs.getbookOrderData() != null)
                        bookOrderModel = prefs.getbookOrderData();
                    else
                        bookOrderModel = new BookOrderModel();
                    bookOrderModel.pickup_address = et_pickup.getText().toString();
                    bookOrderModel.picup_latitude = latitude;
                    bookOrderModel.pickup_longitude = longitude;
                    prefs.setBookOrderData(bookOrderModel);
                    AppDelegate.setDATAfromPrefs = true;
                } else {
                    if (prefs.getbookOrderData() != null)
                        bookOrderModel = prefs.getbookOrderData();
                    else
                        bookOrderModel = new BookOrderModel();
                    bookOrderModel.dropoff_address = et_pickup.getText().toString();
                    bookOrderModel.dropoff_latitude = latitude;
                    bookOrderModel.dropoff_longitude = longitude;
                    prefs.setBookOrderData(bookOrderModel);
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    private void initView() {
        et_pickup = (EditText) findViewById(R.id.et_pickup);
        et_pickup.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        addtextWatcher();
        no_location_text_pickup = (TextView) findViewById(R.id.no_location_text);
        search_list_pickup = (ListView) findViewById(R.id.search_list);
        no_location_text_pickup = (TextView) findViewById(R.id.no_location_text);
        search_list_pickup = (ListView) findViewById(R.id.search_list);
        adapter_pickup = new LocationMapAdapter(ChooseLocationActivity.this, mapPlacesArray_pickup, this);
        search_list_pickup.setAdapter(adapter_pickup);
        pb_c_location_progressbar_pickup = (ProgressBar) findViewById(R.id.pb_c_location_progressbar);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        if (type == PICK_UP) {
            ((android.widget.ImageView) findViewById(R.id.img_green_dot)).setImageResource(R.drawable.green_dot);
            et_pickup.setHint("Choose pick up location");
        } else {
            ((android.widget.ImageView) findViewById(R.id.img_green_dot)).setImageResource(R.drawable.red_dot);
            et_pickup.setHint("Choose drop off location");
        }
        expandableView(exp_layout, COLLAPSE);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.img_clr_txt).setOnClickListener(this);
        findViewById(R.id.img_c_map_pickup).setOnClickListener(this);
        rl_c_triptype = (RelativeLayout) findViewById(R.id.rl_c_triptype);
        rl_c_triptype.setOnClickListener(this);
        img_c_triptype = (ImageView) findViewById(R.id.img_c_triptype);
        rl_c_paymenttype = (RelativeLayout) findViewById(R.id.rl_c_paymenttype);
        rl_c_paymenttype.setOnClickListener(this);
        img_c_paymenttype = (ImageView) findViewById(R.id.img_c_paymenttype);
        rl_c_timetype = (RelativeLayout) findViewById(R.id.rl_c_timetype);
        rl_c_timetype.setOnClickListener(this);
        img_c_timetype = (ImageView) findViewById(R.id.img_c_timetype);
        rl_c_promotype = (RelativeLayout) findViewById(R.id.rl_c_promotype);
        rl_c_promotype.setOnClickListener(this);
        img_c_promotype = (ImageView) findViewById(R.id.img_c_promotype);
        fragmentManager = getSupportFragmentManager();
        setInitailToolBar(0);
        displayView(ICON_TRIP);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void displayView(int position) {
        AppDelegate.hideKeyBoard(this);
        setInitailToolBar(position);
        Fragment fragment = null;
        switch (position) {
            case ICON_PAYMENT:
                fragment = new TransportationListFragment();
                break;
            case ICON_PROMOS:
                fragment = new AttractionListFragment();
                break;
            case ICON_TIME:
                fragment = new HotelsListFragment();
                break;
            case ICON_TRIP:
                fragment = new FavoriteListFragment();
                break;
            default:
                break;
        }
        if (fragment != null) {
            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment, R.id.container2);
        } else if (position != 6) {
            AppDelegate.LogE("Error in creating fragment");
        }
    }

    public void setInitailToolBar(int value) {
        rl_c_paymenttype.setBackgroundColor(getResources().getColor(R.color.white));
        rl_c_promotype.setBackgroundColor(getResources().getColor(R.color.white));
        rl_c_timetype.setBackgroundColor(getResources().getColor(R.color.white));
        rl_c_triptype.setBackgroundColor(getResources().getColor(R.color.white));
        img_c_triptype.setSelected(false);
        img_c_paymenttype.setSelected(false);
        img_c_timetype.setSelected(false);
        img_c_promotype.setSelected(false);
        switch (value) {
            case ICON_PAYMENT:
                rl_c_paymenttype.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_c_paymenttype.setSelected(true);
                break;
            case ICON_PROMOS:
                rl_c_promotype.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_c_promotype.setSelected(true);
                break;
            case ICON_TIME:
                rl_c_timetype.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_c_timetype.setSelected(true);
                break;
            case ICON_TRIP:
                rl_c_triptype.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_c_triptype.setSelected(true);
                break;
        }
    }

    private void addtextWatcher() {
        et_pickup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                AppDelegate.LogT("et_pickup afterTextChanged called");
                mHandler.sendEmptyMessage(5);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                finish();
                break;
            case R.id.rl_c_paymenttype:
                displayView(ICON_PAYMENT);
                break;
            case R.id.rl_c_promotype:
                displayView(ICON_PROMOS);
                break;
            case R.id.rl_c_triptype:
                displayView(ICON_TRIP);
                break;
            case R.id.rl_c_timetype:
                displayView(ICON_TIME);
                break;
            case R.id.img_c_map_pickup:
                Intent intent = new Intent(this, MapActivity.class);
                intent.putExtra(Tags.type, type);
                startActivity(intent);
                break;
            case R.id.img_clr_txt:
                if (type == DROP_OFF) {
                    et_pickup.setText("");
                    if (bookOrderModel != null) {
                        bookOrderModel.dropoff_latitude = 0;
                        bookOrderModel.dropoff_longitude = 0;
                        bookOrderModel.dropoff_address = "";
                        prefs.setBookOrderData(bookOrderModel);
                    }
                } else {
                    et_pickup.setText("");
                    if (bookOrderModel != null) {
                        bookOrderModel.pickup_longitude = 0;
                        bookOrderModel.picup_latitude = 0;
                        bookOrderModel.pickup_address = "";
                        prefs.setBookOrderData(bookOrderModel);
                    }
                }
                break;
        }
    }

    /* Location search */


    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    private void stopStartThread(Thread theThread) {
        stopTimer();
        onlyStopThread(mThreadOnSearch_pickup);
        if (!AppDelegate.haveNetworkConnection(ChooseLocationActivity.this, false)) {
            no_location_text_pickup.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar_pickup.setVisibility(View.INVISIBLE);
            no_location_text_pickup.setVisibility(View.VISIBLE);
            search_list_pickup.setVisibility(View.GONE);
            expandableView(exp_layout, EXPAND);
        } else {
            expandableView(exp_layout, EXPAND);
            mTimer = new Timer();
            mtimerTask = new TimerTask() {
                public void run() {
                    mThreadOnSearch_pickup = new Thread(mTimerRunnable);
                    mThreadOnSearch_pickup.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null) {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (!AppDelegate.haveNetworkConnection(ChooseLocationActivity.this, false)) {
                mHandler.sendEmptyMessage(21);
            } else if (et_pickup.length() == 0) {
                mHandler.sendEmptyMessage(22);
            } else {
                mapPlacesArray_pickup.clear();
                mapPlacesArray_pickup.addAll(PlacesService.autocompleteForMap(et_pickup.getText().toString()));
                mHandler.sendEmptyMessage(4);
                canSearch = true;
            }
        }
    };


    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.hideKeyBoard(ChooseLocationActivity.this);
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(ChooseLocationActivity.this)) {
            canSearch = false;
            if (mapPlacesArray_pickup.size() > position) {
                placeDetail_pickup = mapPlacesArray_pickup.get(position);
                if (AppDelegate.isValidString(placeDetail_pickup.name) && AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_pickup.setText(placeDetail_pickup.name + ", " + placeDetail_pickup.vicinity);
                } else if (AppDelegate.isValidString(placeDetail_pickup.name)) {
                    et_pickup.setText(placeDetail_pickup.name);
                } else if (AppDelegate.isValidString(placeDetail_pickup.vicinity)) {
                    et_pickup.setText(placeDetail_pickup.vicinity);
                }
                et_pickup.setSelection(et_pickup.length());
                expandableView(exp_layout, COLLAPSE);
                mHandler.sendEmptyMessage(10);
                LocationAddress.getLocationFromReverseGeocoder(et_pickup.getText().toString(), ChooseLocationActivity.this, mHandler);
            }
            canSearch = true;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int type) {
    }
}
