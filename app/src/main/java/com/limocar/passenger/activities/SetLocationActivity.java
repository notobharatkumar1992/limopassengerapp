package com.limocar.passenger.activities;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.Async.LocationAddress;
import com.limocar.passenger.Async.PlacesService;
import com.limocar.passenger.R;
import com.limocar.passenger.adapters.PickupDropoffAdapter;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.expandablelayout.ExpandableRelativeLayout;
import com.limocar.passenger.fragments.FavoriteListFragment;
import com.limocar.passenger.interfaces.OnListItemClickListener;
import com.limocar.passenger.models.Place;

import java.util.ArrayList;
import java.util.List;

import carbon.widget.ImageView;
import carbon.widget.ProgressBar;
import carbon.widget.TextView;

import static com.limocar.passenger.fragments.HomeFragment.DROP_OFF;
import static com.limocar.passenger.fragments.HomeFragment.ICON_PAYMENT;
import static com.limocar.passenger.fragments.HomeFragment.ICON_PROMOS;
import static com.limocar.passenger.fragments.HomeFragment.ICON_TIME;
import static com.limocar.passenger.fragments.HomeFragment.ICON_TRIP;
import static com.limocar.passenger.fragments.HomeFragment.PICK_UP;

/**
 * Created by Heena on 10-Oct-16.
 */
public class SetLocationActivity extends AppCompatActivity implements View.OnClickListener, OnListItemClickListener {

    public static Activity mActivity;
    public static final int ADHOC = 1, HERO = 2;
    private Handler mHandler;
    private LinearLayout ll_adhoc_deals;
    public static FragmentManager fragmentManager;
    private ImageView img_c_triptype, img_c_paymenttype, img_c_timetype, img_c_promotype;
    private android.widget.TextView et_pickup, et_dropoff;
    double latitude = 0, longitude = 0;
    RelativeLayout rl_c_triptype, rl_c_paymenttype, rl_c_timetype, rl_c_promotype;
    int type = PICK_UP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        setContentView(R.layout.set_location);
        setHandler();
        initView();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(SetLocationActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SetLocationActivity.this);
                        break;
                    case 1:
                        break;
                    case 2:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        mHandler.sendEmptyMessage(10);
                        LocationAddress.getLocationFromReverseGeocoder(et_pickup.getText().toString(), SetLocationActivity.this, mHandler);
                        break;
                    case 4:
                        AppDelegate.LogT("Handler 4 called");
                        pb_c_location_progressbar.setVisibility(View.INVISIBLE);
                        if (adapter != null && search_list != null) {
                            adapter.notifyDataSetChanged();
                            search_list.invalidate();
                        }
                        if (mapPlacesArray != null && mapPlacesArray.size() > 0) {
                            if (et_pickup.length() > 0) {
                                AppDelegate.LogT("handler 1");
                                search_list.setVisibility(View.VISIBLE);
                                no_location_text.setVisibility(View.GONE);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            search_list.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult) {
                                search_list.setVisibility(View.GONE);
                                no_location_text.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            } else {
                                search_list.setVisibility(View.GONE);
                                no_location_text.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 5:
                        AppDelegate.LogT("Handler 5 called");
                        if (canSearch) {
                            locationSelected = false;
                            if (et_pickup.length() != 0) {
                                pb_c_location_progressbar.setVisibility(View.VISIBLE);
                                stopStartThread(mThreadOnSearch);
                                no_location_text.setVisibility(View.GONE);
                                expandableView(exp_layout, EXPAND);
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        } else {
                            if (et_pickup.length() != 0) {
                            } else {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        break;
                    case 6:
                        mHandler.sendEmptyMessage(10);
                        LocationAddress.getLocationFromReverseGeocoder(et_dropoff.getText().toString(), SetLocationActivity.this, mHandler);
                        break;
                    case 7:
                        AppDelegate.LogT("Handler 7 called");
                        pb_c_location_progressbar1.setVisibility(View.INVISIBLE);
                        if (adapter1 != null && search_list1 != null) {
                            adapter1.notifyDataSetChanged();
                            search_list1.invalidate();
                        }
                        if (mapPlacesArray1 != null && mapPlacesArray1.size() > 0) {
                            if (et_dropoff.length() > 0) {
                                AppDelegate.LogT("handler 7");
                                search_list1.setVisibility(View.VISIBLE);
                                no_location_text1.setVisibility(View.GONE);
                            } else {
                                expandableView1(exp_layout1, COLLAPSE);
                            }
                        } else {
                            search_list1.setVisibility(View.GONE);
                            if (AppDelegate.zeroResultfordropoff) {
                                search_list1.setVisibility(View.GONE);
                                no_location_text1.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            } else {
                                search_list1.setVisibility(View.GONE);
                                no_location_text1.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text1.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar1.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 8:
                        AppDelegate.LogT("Handler 8 called");
                        if (canSearch1) {
                            locationSelected1 = false;
                            if (et_dropoff.length() != 0) {
                                pb_c_location_progressbar1.setVisibility(View.VISIBLE);
                                stopStartThread1(mThreadOnSearch1);
                                no_location_text1.setVisibility(View.GONE);
                                expandableView1(exp_layout1, EXPAND);
                            } else {
                                expandableView1(exp_layout1, COLLAPSE);
                            }
                        } else {
                            if (et_dropoff.length() != 0) {
                            } else {
                                expandableView1(exp_layout1, COLLAPSE);
                            }
                        }
                        break;
                }
            }
        };
    }

    private void getLatLngFromGEOcoder(Bundle data) {
        AppDelegate.LogT("for latlng => " + data.getString(Tags.forLatlng) + ", latitude =" + latitude + ", longitude = " + longitude);
        if (AppDelegate.isValidString(data.getString(Tags.forLatlng)) && data.getString(Tags.forLatlng).equalsIgnoreCase(Tags.forLatlng)) {
            try {
                latitude = data.getDouble(Tags.latitude);
                longitude = data.getDouble(Tags.longitude);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    private void initView() {
        if (getIntent() != null)
            type = getIntent().getIntExtra(Tags.type, PICK_UP);
        et_pickup = (android.widget.TextView) findViewById(R.id.et_pickup);
        et_pickup.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        et_dropoff = (android.widget.TextView) findViewById(R.id.et_dropoff);
        et_dropoff.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_Regular)));
        addtextWatcher();
        no_location_text = (TextView) findViewById(R.id.no_location_text);
        search_list = (ListView) findViewById(R.id.search_list);
        adapter = new PickupDropoffAdapter(SetLocationActivity.this, mapPlacesArray, this, PICK_UP);
        search_list.setAdapter(adapter);
        pb_c_location_progressbar = (ProgressBar) findViewById(R.id.pb_c_location_progressbar);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        no_location_text1 = (TextView) findViewById(R.id.no_location_text1);
        search_list1 = (ListView) findViewById(R.id.search_list1);
        adapter1 = new PickupDropoffAdapter(SetLocationActivity.this, mapPlacesArray, this, DROP_OFF);
        search_list1.setAdapter(adapter1);
        pb_c_location_progressbar1 = (ProgressBar) findViewById(R.id.pb_c_location_progressbar1);
        exp_layout1 = (ExpandableRelativeLayout) findViewById(R.id.exp_layout1);
        expandableView(exp_layout, COLLAPSE);
        expandableView1(exp_layout1, COLLAPSE);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.img_clr_txt_pickup).setOnClickListener(this);
        findViewById(R.id.img_clr_txt_dropoff).setOnClickListener(this);
        rl_c_triptype = (RelativeLayout) findViewById(R.id.rl_c_triptype);
        rl_c_triptype.setOnClickListener(this);
        img_c_triptype = (ImageView) findViewById(R.id.img_c_triptype);
        rl_c_paymenttype = (RelativeLayout) findViewById(R.id.rl_c_paymenttype);
        rl_c_paymenttype.setOnClickListener(this);
        img_c_paymenttype = (ImageView) findViewById(R.id.img_c_paymenttype);
        rl_c_timetype = (RelativeLayout) findViewById(R.id.rl_c_timetype);
        rl_c_timetype.setOnClickListener(this);
        img_c_timetype = (ImageView) findViewById(R.id.img_c_timetype);
        rl_c_promotype = (RelativeLayout) findViewById(R.id.rl_c_promotype);
        rl_c_promotype.setOnClickListener(this);
        img_c_promotype = (ImageView) findViewById(R.id.img_c_promotype);
        fragmentManager = getSupportFragmentManager();
        setInitailToolBar(0);
        displayView(ICON_TRIP);
        if (type == DROP_OFF)
            findViewById(R.id.rl_dropoff).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.rl_dropoff).setVisibility(View.GONE);

    }

    private void displayView(int position) {
        AppDelegate.hideKeyBoard(this);
        setInitailToolBar(position);
        if (position == 1) {
            return;
        }

        Fragment fragment = null;
        switch (position) {
            case ICON_PAYMENT:
                fragment = new FavoriteListFragment();
                break;
            case ICON_PROMOS:
                fragment = new FavoriteListFragment();
                break;
            case ICON_TIME:
                fragment = new FavoriteListFragment();
                break;
            case ICON_TRIP:
                fragment = new FavoriteListFragment();
                break;
            default:
                break;
        }
        if (fragment != null) {
            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment, R.id.container2);
        } else if (position != 6) {
            AppDelegate.LogE("Error in creating fragment");
        }
    }

    public void setInitailToolBar(int value) {
        rl_c_paymenttype.setBackgroundColor(getResources().getColor(R.color.white));
        rl_c_promotype.setBackgroundColor(getResources().getColor(R.color.white));
        rl_c_timetype.setBackgroundColor(getResources().getColor(R.color.white));
        rl_c_triptype.setBackgroundColor(getResources().getColor(R.color.white));
        img_c_triptype.setSelected(false);
        img_c_paymenttype.setSelected(false);
        img_c_timetype.setSelected(false);
        img_c_promotype.setSelected(false);
        switch (value) {
            case ICON_PAYMENT:
                rl_c_paymenttype.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_c_paymenttype.setSelected(true);
                break;
            case ICON_PROMOS:
                rl_c_promotype.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_c_promotype.setSelected(true);
                break;
            case ICON_TIME:
                rl_c_timetype.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_c_timetype.setSelected(true);
                break;
            case ICON_TRIP:
                rl_c_triptype.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                img_c_triptype.setSelected(true);
                break;
        }
    }

    private void addtextWatcher() {
        et_pickup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                findViewById(R.id.rl_dropoff).setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                AppDelegate.LogT("et_pickup afterTextChanged called");
                mHandler.sendEmptyMessage(5);
            }
        });
        et_dropoff.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                AppDelegate.LogT("et_dropoff afterTextChanged called");
                mHandler.sendEmptyMessage(8);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                finish();
                break;
            case R.id.rl_c_paymenttype:
                setInitailToolBar(ICON_PAYMENT);
                break;
            case R.id.rl_c_promotype:
                setInitailToolBar(ICON_PROMOS);
                break;
            case R.id.rl_c_triptype:
                setInitailToolBar(ICON_TRIP);
                break;
            case R.id.rl_c_timetype:
                setInitailToolBar(ICON_TIME);
                break;
            case R.id.img_clr_txt_pickup:
                et_pickup.setText("");
                break;
            case R.id.img_clr_txt_dropoff:
                et_dropoff.setText("");
                break;
        }
    }

    /* Location search */
    public List<Place> mapPlacesArray = new ArrayList<>();
    public List<Place> mapPlacesArray1 = new ArrayList<>();
    public Thread mThreadOnSearch, mThreadOnSearch1;
    public PickupDropoffAdapter adapter, adapter1;
    public ListView search_list, search_list1;
    public TextView no_location_text, no_location_text1;
    public ProgressBar pb_c_location_progressbar, pb_c_location_progressbar1;
    private boolean canSearch = true, canSearch1 = true, locationSelected = false, locationSelected1 = false;
    public Place placeDetail, placeDetail1;
    public ExpandableRelativeLayout exp_layout, exp_layout1;

    public static final int COLLAPSE = 0, EXPAND = 1;

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    public void expandableView1(final ExpandableRelativeLayout expLayout1, final int value) {
        expLayout1.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout1.collapse();
                } else {
                    expLayout1.expand();
                }
            }
        });
    }

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    private void stopStartThread(Thread theThread) {
        if (theThread != null)
            onlyStopThread(theThread);
        if (!AppDelegate.haveNetworkConnection(SetLocationActivity.this, false)) {
            no_location_text.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar.setVisibility(View.INVISIBLE);
            no_location_text.setVisibility(View.VISIBLE);
            search_list.setVisibility(View.GONE);
            expandableView(exp_layout, EXPAND);
        } else {
            expandableView(exp_layout, EXPAND);
            theThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    mapPlacesArray.clear();
                    mapPlacesArray.addAll(PlacesService.autocompleteForMap(et_pickup.getText().toString()));
                    mHandler.sendEmptyMessage(4);
                    canSearch = true;
                }
            });
            theThread.start();
        }
    }

    private void stopStartThread1(Thread theThread) {
        if (theThread != null)
            onlyStopThread(theThread);
        if (!AppDelegate.haveNetworkConnection(SetLocationActivity.this, false)) {
            no_location_text1.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar1.setVisibility(View.INVISIBLE);
            no_location_text1.setVisibility(View.VISIBLE);
            search_list1.setVisibility(View.GONE);
            expandableView1(exp_layout1, EXPAND);
        } else {
            expandableView1(exp_layout1, EXPAND);
            theThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    mapPlacesArray1.clear();
                    mapPlacesArray1.addAll(PlacesService.autocompleteForMap(et_dropoff.getText().toString()));
                    mHandler.sendEmptyMessage(7);
                    canSearch1 = true;
                }
            });
            theThread.start();
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.hideKeyBoard(SetLocationActivity.this);
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(SetLocationActivity.this)) {
            canSearch = false;
            if (mapPlacesArray.size() > position) {
                placeDetail = mapPlacesArray.get(position);
                if (AppDelegate.isValidString(placeDetail.name) && AppDelegate.isValidString(placeDetail.vicinity)) {
                    et_pickup.setText(placeDetail.name + ", " + placeDetail.vicinity);
                } else if (AppDelegate.isValidString(placeDetail.name)) {
                    et_pickup.setText(placeDetail.name);
                } else if (AppDelegate.isValidString(placeDetail.vicinity)) {
                    et_pickup.setText(placeDetail.vicinity);
                }
                canSearch = true;
                locationSelected = true;
                expandableView(exp_layout, COLLAPSE);
            }
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int type) {
        if (type == PICK_UP) {
            AppDelegate.hideKeyBoard(SetLocationActivity.this);
            if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(SetLocationActivity.this)) {
                canSearch = false;
                if (mapPlacesArray.size() > position) {
                    placeDetail = mapPlacesArray.get(position);
                    if (AppDelegate.isValidString(placeDetail.name) && AppDelegate.isValidString(placeDetail.vicinity)) {
                        et_pickup.setText(placeDetail.name + ", " + placeDetail.vicinity);
                    } else if (AppDelegate.isValidString(placeDetail.name)) {
                        et_pickup.setText(placeDetail.name);
                    } else if (AppDelegate.isValidString(placeDetail.vicinity)) {
                        et_pickup.setText(placeDetail.vicinity);
                    }
                    canSearch = true;
                    locationSelected = true;
                    expandableView(exp_layout, COLLAPSE);
                    findViewById(R.id.rl_dropoff).setVisibility(View.VISIBLE);
                }
            }
        } else {
            AppDelegate.hideKeyBoard(SetLocationActivity.this);
            if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(SetLocationActivity.this)) {
                canSearch1 = false;
                if (mapPlacesArray1.size() > position) {
                    placeDetail1 = mapPlacesArray1.get(position);
                    if (AppDelegate.isValidString(placeDetail1.name) && AppDelegate.isValidString(placeDetail1.vicinity)) {
                        et_dropoff.setText(placeDetail1.name + ", " + placeDetail1.vicinity);
                    } else if (AppDelegate.isValidString(placeDetail1.name)) {
                        et_dropoff.setText(placeDetail1.name);
                    } else if (AppDelegate.isValidString(placeDetail1.vicinity)) {
                        et_dropoff.setText(placeDetail1.vicinity);
                    }
                    canSearch1 = true;
                    locationSelected1 = true;
                    expandableView1(exp_layout1, COLLAPSE);
                }
            }
        }
    }
}
