package com.limocar.passenger.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.TransitionHelper;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.net.Callback;
import com.limocar.passenger.net.RestError;
import com.limocar.passenger.net.SingletonRestClient;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Bharat on 12-Jan-17.
 */
public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private MaterialEditText met_username;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.forgot_password_activity);
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ForgotPasswordActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ForgotPasswordActivity.this);
                        break;

                }
            }
        };
    }

    private void initView() {
        met_username = (MaterialEditText) findViewById(R.id.met_username);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        findViewById(R.id.txt_c_sign_in).setOnClickListener(this);
        findViewById(R.id.txt_c_sign_up).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                executeForgotPasswordAsync();

                break;
            case R.id.txt_c_sign_in:
                onBackPressed();
                break;
            case R.id.txt_c_sign_up:
                Intent intent = new Intent(ForgotPasswordActivity.this, SignUpActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(ForgotPasswordActivity.this, false,
                        new Pair<>(findViewById(R.id.txt_c_sign_up), getString(R.string.title_name)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(ForgotPasswordActivity.this, pairs);
                startActivity(intent, transitionActivityOptions.toBundle());
                break;
        }
    }

    private void executeForgotPasswordAsync() {
        if (met_username.length() == 0) {
            AppDelegate.showToast(this, "Please enter username.");
        } else if (!AppDelegate.CheckEmail(met_username.getText().toString())) {
            AppDelegate.showToast(this, "Please enter valid Username.");
        } else if (AppDelegate.haveNetworkConnection(this)) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().userForgetPassword(met_username.getText().toString(), new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(ForgotPasswordActivity.this, obj_json);
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(ForgotPasswordActivity.this, getString(R.string.something_wrong_with_server_response));
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.s).contains("0")) {
                            AppDelegate.showToast(ForgotPasswordActivity.this, obj_json.getString(Tags.m));
                            onBackPressed();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void parseForgotResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt(Tags.s) == 0) {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
