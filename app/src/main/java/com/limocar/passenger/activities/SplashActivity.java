package com.limocar.passenger.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;
import android.view.Window;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.TransitionHelper;
import com.limocar.passenger.utils.Prefs;

public class SplashActivity extends Activity {

    // Global variable declaration
    protected int mSplashTime = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To remove Action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // inflate layout xml file for Splash Activity
        setContentView(R.layout.activity_splash_02);
        new Prefs(this).setBookOrderData(null);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (new Prefs(SplashActivity.this).getUserdata() != null) {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, SignInActivity.class);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SplashActivity.this, false,
                            new Pair<>(findViewById(R.id.img_logo), getString(R.string.img_logo)),
                            new Pair<>(findViewById(R.id.img_right_top), getString(R.string.img_right_top)));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SplashActivity.this, pairs);
                    startActivity(intent, transitionActivityOptions.toBundle());
                    finish();
                }
            }
        }, 1000);
    }

    // when user press the back_black button then only current thread would be interrupted
    @Override
    public void onBackPressed() {
    }

}
