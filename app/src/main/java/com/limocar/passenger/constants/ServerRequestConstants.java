package com.limocar.passenger.constants;

public class ServerRequestConstants {
    /**
     * (All Constants that were used in server connection are declared)
     */
    /*
     * Post Parameter Type Constants
	 */
    public static final String CODE_200 = "200";
    public static final String CODE_400 = "400";
    public static final String CODE_401 = "401";
    public static final String CODE_402 = "402";
    public static final String CODE_403 = "403";
    public static final String CODE_404 = "404";

    public static final String Key_PostStrValue = "String";
    public static final String Key_PostDoubleValue = "double";
    public static final String Key_PostintValue = "int";
    public static final String Key_PostbyteValue = "byte";
    public static final String Key_PostFileValue = "File";
    public static final String Key_PostStringArrayValue = "String[]";
    public static final String Key_PostStringArrayValue1 = "String[]";

    //    public static final String CHAT_SERVER_URL = "http://192.168.100.38:3000";
    public static final String BASE_URL = "http://notosolutions.net/craftdeals/webServices/";
    public static final String GET_DASHBOARD = BASE_URL + "getDashboard";
    public static final String GET_ADHOC_DEALS = BASE_URL + "getAdhocList";
    public static final String GET_HERO_DEALS = BASE_URL + "getHeroList";
    public static final String SEARCH = BASE_URL + "search";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgotPassword";
    public static final String ADHOC_LIKE = BASE_URL + "adhocfavorites";
    public static final String VENDOR_PROFILE = BASE_URL + "vendorprofile";
    public static final String LOGIN = BASE_URL + "login";
    public static final String SIGN_UP = BASE_URL + "signUp";
    public static final String CHK_FACEBOOK = BASE_URL + "check_facebook";
    public static final String SIGNUP_FACEBOOK = BASE_URL + "signup_facebook";
    public static final String GET_MEMBERSHIP_LIST = BASE_URL + "getMembershipList";
    public static final String GET_ADHOC_DETAILS = BASE_URL + "getAdhocDetails";

    public static final String MY_PURCHASE = BASE_URL + "myPurchase";
    public static final String MY_HERODEALS = BASE_URL + "myHeroDealList";
    public static final String MY_FAVOURITE_DEALS = BASE_URL + "myFavorite";
    public static final String CITY_LIST = BASE_URL + "getCityList";
    public static final String GET_BEVERAGES = BASE_URL + "getBevrages";
    public static final String GET_CITY = BASE_URL + "getCity";
    public static final String GET_PROVINCE = BASE_URL + "getStateList";
    public static final String GET_MYPROFILE = BASE_URL + "getMyProfile";
    public static final String EDIT_PROFILE = BASE_URL + "updateUser";
    public static final String REDEEM_DEAL = BASE_URL + "redeemHeroDeal";

    public static final String EMAIL_SUBSCRIPTION = BASE_URL + "emailSubscription";
    public static final String CONFIRM_ORDER = BASE_URL + "confirmOrder";
    public static final String CHANGE_PASSWORD = BASE_URL + "changePassword";
    public static final String REDEEM_ADHOC = BASE_URL + "redeemAdhocDeal";

    public static final String UPGRADE_MEMBERSHIP = BASE_URL + "upgradeMembership";
    public static final String UPDATE_RANGE = BASE_URL + "updateUserRange";

    public static final String GET_PURCHASE_DATA = BASE_URL + "myPurchaseDetail";
    public static final String GET_REDEEMED_HERO_DEAL_DATA = BASE_URL + "redeemedHeroDetail";

    public static final String TERMS_CONDITIONS = "http://notosolutions.net/craftdeals/terms.html";
    public static final String FAQ = "http://notosolutions.net/craftdeals/faq.html";
    public static final String PRIVACY_POLICY = "http://notosolutions.net/craftdeals/privacy-policy.html";
    public static final String HELP_PAEG = "http://notosolutions.net/craftdeals/help.html";
    public static final String GET_NOTIFICATION = BASE_URL + "getNotification";
    public static final String VIEW_NOTIFICATION = BASE_URL + "/passenger/profile/update";
}

