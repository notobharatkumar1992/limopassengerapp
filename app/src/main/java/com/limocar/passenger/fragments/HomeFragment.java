package com.limocar.passenger.fragments;//package com.limocar.passenger.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.Async.GetKMAsync;
import com.limocar.passenger.Async.LocationAddress;
import com.limocar.passenger.R;
import com.limocar.passenger.activities.ChooseLocationActivity;
import com.limocar.passenger.activities.MainActivity;
import com.limocar.passenger.adapters.TaxiTypeAdapter;
import com.limocar.passenger.constants.Constants;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.interfaces.AsyncGetDirection;
import com.limocar.passenger.interfaces.OnListItemClickListener;
import com.limocar.passenger.models.BookOrderModel;
import com.limocar.passenger.models.TaxiTypeModel;
import com.limocar.passenger.net.Callback;
import com.limocar.passenger.net.RestError;
import com.limocar.passenger.net.SingletonRestClient;
import com.limocar.passenger.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import carbon.widget.EditText;
import carbon.widget.ImageView;
import carbon.widget.TextView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


/**
 * Created by Heena on 07/29/2016.
 */
public class HomeFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, View.OnClickListener, AsyncGetDirection, OnListItemClickListener {

    private Handler mHandler;
    ImageView img_c_triptype, img_c_paymenttype, img_c_timetype, img_c_promotype;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;
    private SupportMapFragment fragment;
    private GoogleMap googleMap;
    android.widget.RelativeLayout rl_fare;
    TextView txt_c_distance, txt_c_fare, txt_c_pick_up, txt_c_drop_off, txt_c_notes_to_driver, txt_c_triptype, txt_c_paymenttype, txt_c_timetype, txt_c_promotype;
    RecyclerView recycler_taxi_type;
    ArrayList<TaxiTypeModel> taxiTypeModelArrayList;
    private TaxiTypeAdapter mAdapter;
    public static final int ICON_TRIP = 0, ICON_PAYMENT = 1, ICON_TIME = 2, ICON_PROMOS = 3;
    public static final int PICK_UP = 0, DROP_OFF = 1;
    public BookOrderModel bookOrderModel;
    public Prefs prefs;
    public android.widget.RelativeLayout rl_book;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Constants.isMapScreen = true;
        prefs = new Prefs(getActivity());
        bookOrderModel = prefs.getbookOrderData();
        initView(view);
        ((MainActivity) getActivity()).setInitailSideBar(MainActivity.PANEL_HOME);
        initGPS();
        showGPSalert();
        setHandler();
        mHandler.sendEmptyMessage(3);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGoogleApiClient.connect();
    }

    private void showGPSalert() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        mHandler.sendEmptyMessage(2);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(
                                    getActivity(), 1000);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    private void initGPS() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                        HomeFragment.this);
                mGoogleApiClient.disconnect();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView(View view) {
        txt_c_triptype = (TextView) view.findViewById(R.id.txt_c_triptype);
        txt_c_paymenttype = (TextView) view.findViewById(R.id.txt_c_paymenttype);
        txt_c_timetype = (TextView) view.findViewById(R.id.txt_c_timetype);
        txt_c_promotype = (TextView) view.findViewById(R.id.txt_c_promotype);
        img_c_triptype = (ImageView) view.findViewById(R.id.img_c_triptype);
        img_c_paymenttype = (ImageView) view.findViewById(R.id.img_c_paymenttype);
        img_c_timetype = (ImageView) view.findViewById(R.id.img_c_timetype);
        img_c_promotype = (ImageView) view.findViewById(R.id.img_c_promotype);
        txt_c_notes_to_driver = (TextView) view.findViewById(R.id.txt_c_notes_to_driver);
        txt_c_notes_to_driver.setOnClickListener(this);
        img_c_triptype.setOnClickListener(this);
        img_c_paymenttype.setOnClickListener(this);
        img_c_timetype.setOnClickListener(this);
        img_c_promotype.setOnClickListener(this);
        rl_book = (android.widget.RelativeLayout) view.findViewById(R.id.rl_book);
        rl_book.setVisibility(View.GONE);
        view.findViewById(R.id.img_c_book).setOnClickListener(this);
        txt_c_distance = (TextView) view.findViewById(R.id.txt_c_distance);
        txt_c_fare = (TextView) view.findViewById(R.id.txt_c_fare);
        rl_fare = (android.widget.RelativeLayout) view.findViewById(R.id.rl_fare);
        view.findViewById(R.id.img_clr_txt_pickup).setOnClickListener(this);
        view.findViewById(R.id.img_clr_txt_dropoff).setOnClickListener(this);
        txt_c_pick_up = (TextView) view.findViewById(R.id.txt_c_pick_up);
        txt_c_drop_off = (TextView) view.findViewById(R.id.txt_c_drop_off);
        txt_c_drop_off.setOnClickListener(this);
        txt_c_pick_up.setOnClickListener(this);
        recycler_taxi_type = (RecyclerView) view.findViewById(R.id.recycler_taxi_type);
        recycler_taxi_type.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recycler_taxi_type.setHasFixedSize(true);
        recycler_taxi_type.setItemAnimator(new DefaultItemAnimator());
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            fragment = SupportMapFragment.newInstance();
                            getChildFragmentManager().beginTransaction()
                                    .replace(R.id.map_container, fragment, "MAP1").addToBackStack(null)
                                    .commit();
                            fragment.getMapAsync(
                                    new OnMapReadyCallback() {
                                        @Override
                                        public void onMapReady(GoogleMap googleMap) {
                                            HomeFragment.this.googleMap = googleMap;
                                            showMap();
                                        }
                                    }
                            );
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                    }
                }
                , 300);
    }

    private void showMap() {
        if (googleMap == null) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        AppDelegate.zoomControlAtTopLeft(fragment.getView());
        googleMap.clear();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        getTaxiType();
                        break;
                    case 1:
                        mAdapter.notifyDataSetChanged();
                        recycler_taxi_type.invalidate();
                        break;
                }
            }
        };
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase("state")) {
            if (taxiTypeModelArrayList != null && taxiTypeModelArrayList.size() > 0) {
                taxiTypeModelArrayList.get(position).isSelected = true;
                bookOrderModel.serviceCategoryCode = taxiTypeModelArrayList.get(position).cat;
                prefs.setBookOrderData(bookOrderModel);
                for (int i = 0; i < taxiTypeModelArrayList.size(); i++) {
                    if (i != position) {
                        taxiTypeModelArrayList.get(i).isSelected = false;
                    }
                }
                mHandler.sendEmptyMessage(1);
            }
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int PICK_UP) {

    }

    public void iconSelector(int value) {
        txt_c_triptype.setSelected(false);
        txt_c_paymenttype.setSelected(false);
        txt_c_timetype.setSelected(false);
        txt_c_promotype.setSelected(false);
        img_c_triptype.setSelected(false);
        img_c_paymenttype.setSelected(false);
        img_c_timetype.setSelected(false);
        img_c_promotype.setSelected(false);
        switch (value) {
            case ICON_PAYMENT:
                img_c_paymenttype.setSelected(true);
                txt_c_paymenttype.setSelected(true);
                break;
            case ICON_PROMOS:
                img_c_promotype.setSelected(true);
                txt_c_promotype.setSelected(true);
                break;
            case ICON_TIME:
                img_c_timetype.setSelected(true);
                txt_c_timetype.setSelected(true);
                break;
            case ICON_TRIP:
                img_c_triptype.setSelected(true);
                txt_c_triptype.setSelected(true);
                break;
        }
    }

    private void getTaxiType() {
        if (AppDelegate.haveNetworkConnection(getActivity())) {
            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getTaxiType(new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(getActivity(), obj_json);
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));

                        if (AppDelegate.isValidString(obj_json.getString(Tags.d))) {
                            taxiTypeModelArrayList = new ArrayList<TaxiTypeModel>();
                            JSONArray object = obj_json.getJSONArray(Tags.d);
                            for (int i = 0; i < object.length(); i++) {
                                JSONObject jsonObject = object.getJSONObject(i);
                                TaxiTypeModel taxiTypeModel = new TaxiTypeModel();
                                taxiTypeModel.display_content = jsonObject.getString(Tags.display_content);
                                taxiTypeModel.display_name = jsonObject.getString(Tags.display_name);
                                taxiTypeModel.icon = jsonObject.getString(Tags.icon);
                                taxiTypeModel.cat = jsonObject.getString(Tags.cat);
                                taxiTypeModelArrayList.add(taxiTypeModel);
                            }
                            showTaxiType(taxiTypeModelArrayList);
                        } else {
                            AppDelegate.checkJsonMessage(getActivity(), obj_json);

                        }
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                }
            });
        }
    }

    private void showTaxiType(ArrayList<TaxiTypeModel> taxiTypeModelArrayList) {
        if (taxiTypeModelArrayList.size() > 0) {
            taxiTypeModelArrayList.get(0).isSelected = true;
            bookOrderModel.serviceCategoryCode = taxiTypeModelArrayList.get(0).cat;
            prefs.setBookOrderData(bookOrderModel);
        }
        mAdapter = new TaxiTypeAdapter(getActivity(), taxiTypeModelArrayList, this);
        recycler_taxi_type.setAdapter(mAdapter);
    }

    private void setAddressFromGEOcoder(Bundle data) {
        if (data != null) {
            currentLatitude = data.getDouble(Tags.LAT);
            currentLongitude = data.getDouble(Tags.LNG);
            if (prefs.getbookOrderData() != null) {
                bookOrderModel = prefs.getbookOrderData();
                if (AppDelegate.isValidString(bookOrderModel.pickup_address)) {
                    txt_c_pick_up.setText(bookOrderModel.pickup_address);
                } else {
                    txt_c_pick_up.setText(data.getString(Tags.ADDRESS));
                    bookOrderModel.pickup_address = txt_c_pick_up.getText().toString();
                    bookOrderModel.picup_latitude = currentLatitude;
                    bookOrderModel.pickup_longitude = currentLongitude;
                    prefs.setBookOrderData(bookOrderModel);
                }
                txt_c_drop_off.setText(bookOrderModel.dropoff_address);
            } else {
                txt_c_pick_up.setText(data.getString(Tags.ADDRESS));
                if (prefs.getbookOrderData() != null)
                    bookOrderModel = prefs.getbookOrderData();
                else
                    bookOrderModel = new BookOrderModel();
                bookOrderModel.pickup_address = txt_c_pick_up.getText().toString();
                bookOrderModel.picup_latitude = currentLatitude;
                bookOrderModel.pickup_longitude = currentLongitude;
                prefs.setBookOrderData(bookOrderModel);
            }
            createMarker();
        } else {
            AppDelegate.LogT("Data receive null.");
        }
    }

    public void updateData() {
        try {
            if (prefs.getbookOrderData() != null) {
                bookOrderModel = prefs.getbookOrderData();
                if (AppDelegate.isValidString(bookOrderModel.pickup_address)) {
                    txt_c_pick_up.setText(bookOrderModel.pickup_address);
                } else {
                    txt_c_pick_up.setText(bookOrderModel.pickup_address);
                    bookOrderModel.pickup_address = txt_c_pick_up.getText().toString();
                    bookOrderModel.picup_latitude = currentLatitude;
                    bookOrderModel.pickup_longitude = currentLongitude;
                    prefs.setBookOrderData(bookOrderModel);
                }
                txt_c_drop_off.setText(bookOrderModel.dropoff_address);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, HomeFragment.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            setloc(currentLatitude, currentLongitude);
            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
        }
    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, getActivity(), mHandler);
    }

    public void createMarker() {
        AppDelegate.LogT("Create Marker called");
        AppDelegate.LogT("currentLatitude out=>" + currentLatitude + ", currentLongitudeout==>" + currentLongitude);
        if (googleMap == null)
            return;

        View markerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        final android.widget.ImageView cimg_user = (android.widget.ImageView) markerView.findViewById(R.id.imageView);
        cimg_user.setImageResource(R.drawable.marker_green);
        try {
            AppDelegate.LogT("Create Marker called4");
            CameraPosition cameraPosition = null;
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            if (prefs.getbookOrderData() != null) {
                googleMap.clear();
                AppDelegate.LogT("Create Marker called5");
                Marker markerObject = googleMap.addMarker(new MarkerOptions().position(new LatLng(prefs.getbookOrderData().picup_latitude, prefs.getbookOrderData().pickup_longitude))
                        .title(AppDelegate.isValidString(prefs.getbookOrderData().pickup_address) ? prefs.getbookOrderData().pickup_address : "PICK UP")
                        .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(getActivity(), markerView))));
                cimg_user.setImageResource(R.drawable.marker_orange);
                builder.include(markerObject.getPosition());
                if (prefs.getbookOrderData().dropoff_latitude != 0 && prefs.getbookOrderData().dropoff_longitude != 0) {
                    markerObject = googleMap.addMarker(new MarkerOptions().position(new LatLng(prefs.getbookOrderData().dropoff_latitude, prefs.getbookOrderData().dropoff_longitude))
                            .title(AppDelegate.isValidString(prefs.getbookOrderData().dropoff_address) ? prefs.getbookOrderData().dropoff_address : "DROP OFF")
                            .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(getActivity(), markerView))));
                    builder.include(markerObject.getPosition());
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 35));
                } else {
                    AppDelegate.LogT("Create Marker called6");
                    cameraPosition = new CameraPosition.Builder().target(new LatLng(prefs.getbookOrderData().picup_latitude, prefs.getbookOrderData().pickup_longitude))
                            .zoom(15.0f).build();
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    AppDelegate.LogT("Create Marker called7==>" + prefs.getbookOrderData().picup_latitude);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(getActivity());
        iconSelector(ICON_TRIP);
        setValues();
    }

    double lastPickup_latitude = 0, lastPickup_longitude = 0;
    double lastDrop_latitude = 0, lastDrop_longitude = 0;

    private void setValues() {
        if (prefs.getbookOrderData() != null) {
            bookOrderModel = prefs.getbookOrderData();
            if (AppDelegate.isValidString(bookOrderModel.pickup_address)) {
                txt_c_pick_up.setText(bookOrderModel.pickup_address);
            }
            if (AppDelegate.isValidString(bookOrderModel.dropoff_address)) {
                txt_c_drop_off.setText(bookOrderModel.dropoff_address);
            }
            createMarker();
            if (bookOrderModel.picup_latitude != 0 && bookOrderModel.pickup_longitude != 0 && bookOrderModel.dropoff_latitude != 0 && bookOrderModel.dropoff_longitude != 0) {
                executeEstimateApi();
            }
        }
    }

    private boolean matchWithLastLocation() {
        boolean isDiff = false;
        if (bookOrderModel.picup_latitude != lastPickup_latitude) {
            isDiff = true;
        } else if (bookOrderModel.pickup_longitude != lastPickup_longitude) {
            isDiff = true;
        } else if (bookOrderModel.dropoff_latitude != lastDrop_latitude) {
            isDiff = true;
        } else if (bookOrderModel.dropoff_longitude != lastDrop_longitude) {
            isDiff = true;
        }
        lastPickup_latitude = bookOrderModel.picup_latitude;
        lastPickup_longitude = bookOrderModel.pickup_longitude;
        lastDrop_latitude = bookOrderModel.dropoff_latitude;
        lastDrop_longitude = bookOrderModel.dropoff_longitude;
        return isDiff;
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        try {
            LocationAddress.getAddressFromLocation(currentLatitude, currentLongitude, getActivity(), mHandler);
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, HomeFragment.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                break;
            case R.id.img_clr_txt_dropoff:
                txt_c_drop_off.setText("");
                if (bookOrderModel != null) {
                    bookOrderModel.dropoff_latitude = 0;
                    bookOrderModel.dropoff_longitude = 0;
                    bookOrderModel.dropoff_address = "";
                    prefs.setBookOrderData(bookOrderModel);
                }
                break;
            case R.id.img_clr_txt_pickup:
                txt_c_pick_up.setText("");
                if (bookOrderModel != null) {
                    bookOrderModel.pickup_longitude = 0;
                    bookOrderModel.picup_latitude = 0;
                    bookOrderModel.pickup_address = "";
                    prefs.setBookOrderData(bookOrderModel);
                }
                break;
            case R.id.img_c_paymenttype:
                iconSelector(ICON_PAYMENT);
                break;
            case R.id.img_c_promotype:
                iconSelector(ICON_PROMOS);
                break;
            case R.id.img_c_triptype:
                iconSelector(ICON_TRIP);
                tripTypeSelectorList();
                break;
            case R.id.img_c_timetype:
                iconSelector(ICON_TIME);
                timeSelectorList();
                break;
            case R.id.txt_c_drop_off:
                Intent intent = new Intent(getActivity(), ChooseLocationActivity.class);
                intent.putExtra(Tags.type, DROP_OFF);
                startActivity(intent);
                break;
            case R.id.txt_c_pick_up:
                intent = new Intent(getActivity(), ChooseLocationActivity.class);
                intent.putExtra(Tags.type, PICK_UP);
                startActivity(intent);
                break;
            case R.id.txt_c_notes_to_driver:
                showNotestoDriverDialog();
                break;
            case R.id.img_c_book:
                executeBookOrder();
                break;
        }
    }

    private void executeBookOrder() {
        bookOrderModel.paymenttype = Tags.CASH;
        prefs.setBookOrderData(bookOrderModel);
        if (!AppDelegate.isValidString(bookOrderModel.triptype))
            AppDelegate.showToast(getActivity(), "Please select trip type.");
        else if (!AppDelegate.isValidString(bookOrderModel.pickup_time_stamp))
            AppDelegate.showToast(getActivity(), "Please select time.");
        else {
            executeBookOrderApi();
        }
    }

    public void tripTypeSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"  PERSONAL", "  BUSINESS", "  CANCEL"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        builder.setTitle("Please select trip type.\n ");
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        switch (i) {
                            case 0:
                                dialog.dismiss();
                                txt_c_triptype.setText("PERSONAL");
                                bookOrderModel.triptype = "PERSONAL";
                                prefs.setBookOrderData(bookOrderModel);
                                break;
                            case 1:
                                dialog.dismiss();
                                txt_c_triptype.setText("BUSINESS");
                                bookOrderModel.triptype = "BUSINESS";
                                prefs.setBookOrderData(bookOrderModel);
                                break;
                            case 2:
                                dialog.dismiss();
                                break;
                        }
                    }
                }
        );
        dialog.show();
    }

    public void timeSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"  NOW", "  FUTURE", "  CANCEL"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        builder.setTitle("Please select time.\n\n");
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        switch (i) {
                            case 0:
                                dialog.dismiss();
                                txt_c_timetype.setText("NOW");
                                bookOrderModel.pickup_type = BookOrderModel.PICKUP_NOW;
                                bookOrderModel.pickup_time_stamp = (System.currentTimeMillis() / 1000) + "";
                                prefs.setBookOrderData(bookOrderModel);
                                break;
                            case 1:
                                dialog.dismiss();
                                showDateDialog();
                                break;
                            case 2:
                                dialog.dismiss();
                                break;
                        }
                    }
                }
        );
        dialog.show();
    }

    private int mYear, mMonth, mDay, mHour, mMinute;

    private void showDateDialog() {
        final Calendar c = Calendar.getInstance();
        if (bookOrderModel.pickup_type == BookOrderModel.PICKUP_FUTURE && AppDelegate.isValidString(bookOrderModel.pickup_date)) {
            c.setTime(getDateObject(bookOrderModel.pickup_date, DATE_FORMAT));
        }
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox
                        bookOrderModel.pickup_date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        bookOrderModel.pickup_time = "";
                        showTimeDialog();
                    }
                }, mYear, mMonth, mDay);
        dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 10000);
        dpd.show();
    }

    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String TIME_FORMAT_12_HOUR = "hh:mm aa";
    public static final String TIME_FORMAT_24_HOUR = "HH:mm";

    public static Date getDateObject(String date, String date_format) {
        try {
            return new SimpleDateFormat(date_format).parse(date);
        } catch (ParseException e) {
            AppDelegate.LogE(e);
        }
        return Calendar.getInstance().getTime();
    }

    public void showTimeDialog() {
        final Calendar c = Calendar.getInstance();
        if (AppDelegate.isValidString(bookOrderModel.pickup_time)) {
            c.setTime(getDateObject(bookOrderModel.pickup_time, TIME_FORMAT_12_HOUR));
        }
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        // Launch Time Picker Dialog
        final TimePickerDialog tpd = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (AppDelegate.isValidString(bookOrderModel.pickup_date)) {
                            try {
                                bookOrderModel.pickup_time = hourOfDay + ":" + minute;
                                txt_c_timetype.setText("FUTURE");
                                bookOrderModel.pickup_type = BookOrderModel.PICKUP_FUTURE;
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(new SimpleDateFormat("dd-MM-yyyy").parse(bookOrderModel.pickup_date));
                                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                calendar.set(Calendar.MINUTE, minute);
                                AppDelegate.LogT("calendar => " + calendar.getTime());
                                bookOrderModel.pickup_time_stamp = (calendar.getTimeInMillis() / 1000) + "";
                                AppDelegate.LogT("calendar timestamp => " + bookOrderModel.pickup_time_stamp);
                                prefs.setBookOrderData(bookOrderModel);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        } else {
                            AppDelegate.showAlert(getActivity(), "Please select pickup date.");
                        }
                    }
                }
                , mHour, mMinute, false);
        tpd.show();
    }


    private void executeEstimateApi() {
        bookOrderModel = prefs.getbookOrderData();
        if (bookOrderModel != null) {
            if (bookOrderModel.picup_latitude != 0 && bookOrderModel.pickup_longitude != 0 && bookOrderModel.dropoff_longitude != 0 && bookOrderModel.dropoff_latitude != 0 /*&& AppDelegate.isValidString(bookOrderModel.serviceCategoryCode)*/) {
                if (AppDelegate.haveNetworkConnection(getActivity())) {
                    mHandler.sendEmptyMessage(10);
                    DecimalFormat df = new DecimalFormat(".#####");
                    AppDelegate.LogT("serviceCategoryCode=" + bookOrderModel.serviceCategoryCode + ", picup_latitude=" + df.format(bookOrderModel.picup_latitude) + ", pickup_longitude=" + bookOrderModel.pickup_longitude + ", dropoff_latitude=" + bookOrderModel.dropoff_latitude + ", dropoff_longitude=" + bookOrderModel.dropoff_longitude + ", pickup_type=" + bookOrderModel.pickup_type + ", pickup_time_stamp=" + bookOrderModel.pickup_time_stamp + "");
                    SingletonRestClient.get().getTheQuoteForTrip("ECO", df.format(bookOrderModel.picup_latitude), df.format(bookOrderModel.pickup_longitude), df.format(bookOrderModel.dropoff_latitude), df.format(bookOrderModel.dropoff_longitude), bookOrderModel.pickup_type + "", bookOrderModel.pickup_time_stamp, new Callback<Response>() {
                        @Override
                        public void failure(RestError restError) {
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            super.failure(error);
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                AppDelegate.checkJsonMessage(getActivity(), obj_json);
                            } catch (JSONException e) {
                                AppDelegate.LogE(e);
                            }
                        }

                        @Override
                        public void success(Response response, Response response2) {
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));

                                if (AppDelegate.isValidString(obj_json.getString(Tags.d))) {
                                    txt_c_fare.setText("Fare : SGD" + obj_json.getJSONObject(Tags.d).getString(Tags.price) + "");
                                    new GetKMAsync(getActivity(), HomeFragment.this, bookOrderModel.picup_latitude, bookOrderModel.pickup_longitude, bookOrderModel.dropoff_latitude, bookOrderModel.dropoff_longitude).execute();
                                } else {
                                    mHandler.sendEmptyMessage(11);
                                    AppDelegate.checkJsonMessage(getActivity(), obj_json);

                                }
                            } catch (JSONException e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    });
                }
            }
        }
    }

    private void executeBookOrderApi() {
        bookOrderModel = prefs.getbookOrderData();
        if (bookOrderModel != null) {
            if (bookOrderModel.picup_latitude != 0 && bookOrderModel.pickup_longitude != 0 && bookOrderModel.dropoff_longitude != 0 && bookOrderModel.dropoff_latitude != 0 /*&& AppDelegate.isValidString(bookOrderModel.serviceCategoryCode)*/) {
                if (AppDelegate.haveNetworkConnection(getActivity())) {
                    mHandler.sendEmptyMessage(10);
                    DecimalFormat df = new DecimalFormat(".#####");
                    AppDelegate.LogT("serviceCategoryCode=" + bookOrderModel.serviceCategoryCode + ", picup_latitude=" + df.format(bookOrderModel.picup_latitude) + ", pickup_longitude=" + bookOrderModel.pickup_longitude + ", dropoff_latitude=" + bookOrderModel.dropoff_latitude + ", dropoff_longitude=" + bookOrderModel.dropoff_longitude + ", pickup_type=" + bookOrderModel.pickup_type + ", pickup_time_stamp=" + bookOrderModel.pickup_time_stamp + "");
                    SingletonRestClient.get().bookOrder("ECO", df.format(bookOrderModel.picup_latitude), df.format(bookOrderModel.pickup_longitude), df.format(bookOrderModel.dropoff_latitude), df.format(bookOrderModel.dropoff_longitude), bookOrderModel.triptype, "", bookOrderModel.paymenttype, bookOrderModel.promotype, txt_c_notes_to_driver.getText().toString(), "", "", new Prefs(getActivity()).getUserdata().first_name, new Prefs(getActivity()).getUserdata().last_name, new Prefs(getActivity()).getUserdata().phone, new Prefs(getActivity()).getUserdata().email, new Callback<Response>() {
                        @Override
                        public void failure(RestError restError) {
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            super.failure(error);
                            mHandler.sendEmptyMessage(11);
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                                AppDelegate.checkJsonMessage(getActivity(), obj_json);
                            } catch (JSONException e) {
                                AppDelegate.LogE(e);
                            }
                        }

                        @Override
                        public void success(Response response, Response response2) {
                            try {
                                JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));

                                if (AppDelegate.isValidString(obj_json.getString(Tags.d))) {
                                    AppDelegate.showToast(getActivity(), obj_json.getString(Tags.m));
                                } else {
                                    mHandler.sendEmptyMessage(11);
                                    AppDelegate.checkJsonMessage(getActivity(), obj_json);

                                }
                            } catch (JSONException e) {
                                AppDelegate.LogE(e);
                            }
                        }
                    });
                }
            }
        }
    }

    public String str_notesToDriver = "";

    public void showNotestoDriverDialog() {
        /* Alert Dialog Code Start*/
        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Notes To Driver"); //Set Alert dialog title here
        alert.setCancelable(false);

        // Set an EditText view to get user input
        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        input.setLayoutParams(params);
        input.setPadding(AppDelegate.dpToPix(getActivity(), 25), AppDelegate.dpToPix(getActivity(), 15), AppDelegate.dpToPix(getActivity(), 25), 0);
        input.setHint("NOTES TO DRIVER");
        input.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_Regular)));
        input.setFocusable(true);
        input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        input.setText(str_notesToDriver);
        input.setSelection(input.length());
        input.setFocusableInTouchMode(true);
        input.setEnabled(true);
        input.setTextColor(getActivity().getResources().getColor(R.color.txt_color));
        alert.setView(input);


        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                str_notesToDriver = input.getText().toString();
                txt_c_notes_to_driver.setText(str_notesToDriver);
            }
        });
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    @Override
    public void OnDirectionCompleted(ArrayList<LatLng> listLatLng, String state) {
        mHandler.sendEmptyMessage(11);
        try {
//            JSONObject jsonObject = new JSONObject(state);
            txt_c_distance.setText("Distance : " + state/*jsonObject.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getString("text") + ""*/);
            rl_book.setVisibility(View.VISIBLE);
//        } catch (JSONException e) {
//            AppDelegate.LogE(e);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        AppDelegate.LogR("OnDirectionCompleted => " + state);
    }
}
