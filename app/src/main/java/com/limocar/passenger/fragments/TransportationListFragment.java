package com.limocar.passenger.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.adapters.AttractionAdapter;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.interfaces.OnLoadMoreListener;
import com.limocar.passenger.models.AttractionModel;
import com.limocar.passenger.net.Callback;
import com.limocar.passenger.net.RestError;
import com.limocar.passenger.net.SingletonRestClient;
import com.limocar.passenger.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class TransportationListFragment extends Fragment {
    RecyclerView recycler_view;
    ArrayList<AttractionModel> attractionModelArrayList = new ArrayList<>();
    private AttractionAdapter mAdapter;
    String next_page_token = "";
    private Handler mHandler;

    public TransportationListFragment() {
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 1:
                        mAdapter.notifyDataSetChanged();
                        break;
                }
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        setHandler();
        executeAttractionAsync();
    }

    private void initView(View view) {
        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_view.setHasFixedSize(true);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
//        setAttractionListAdapter();
        mAdapter = new AttractionAdapter(getActivity(), attractionModelArrayList, recycler_view);
        recycler_view.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("haint", "Load More");
                attractionModelArrayList.add(null);
                mAdapter.notifyItemInserted(attractionModelArrayList.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("haint", "Load More 2");
                        //Remove loading item
                        attractionModelArrayList.remove(attractionModelArrayList.size() - 1);
                        mAdapter.notifyItemRemoved(attractionModelArrayList.size());
                        //Load data
                        executeAttractionAsync();
                        AppDelegate.LogT("Tansport Array List==>"+attractionModelArrayList.size());
//                        int index = attractionModelArrayList.size();
//                        int end = index + 20;
//                        for (int i = index; i < end; i++) {
//                            User user = new User();
//                            user.setName("Name " + i);
//                            user.setEmail("alibaba" + i + "@gmail.com");
//                            mUsers.add(user);
//                        }
//                        mAdapter.notifyDataSetChanged();
//                        mAdapter.setLoaded();
                    }
                }, 5000);
            }
        });
    }

    private void executeAttractionAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity())) {
            if (!AppDelegate.isValidString(next_page_token) && attractionModelArrayList.size()==0)
                mHandler.sendEmptyMessage(10);
//            mHandler.sendEmptyMessage(10);
            SingletonRestClient.get().getTransport(next_page_token, new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void failure(RetrofitError error) {
                    super.failure(error);
                    mHandler.sendEmptyMessage(11);
                    try {
                        JSONObject obj_json = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                        AppDelegate.checkJsonMessage(getActivity(), obj_json);
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                        AppDelegate.showToast(getActivity(), getString(R.string.something_wrong_with_server_response));
                    }
                }

                @Override
                public void success(Response response, Response response2) {
                    mHandler.sendEmptyMessage(11);
                    JSONObject obj_json = null;
                    try {
                        obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                        if (obj_json.getString(Tags.s).contains("0")) {
//                            AppDelegate.showToast(getActivity(), obj_json.getString(Tags.m));
                            JSONObject object = obj_json.getJSONObject(Tags.d);
                            if (object.has(Tags.next_page_token))
                                next_page_token = JSONParser.getString(object, Tags.next_page_token);
                            else
                                next_page_token = "";
                            JSONArray jsonArray = object.getJSONArray(Tags.results);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                AttractionModel attractionModel = new AttractionModel();
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if (jsonObject.has(Tags.formatted_address))
                                    attractionModel.formatted_address = JSONParser.getString(jsonObject, Tags.formatted_address);
                                if (jsonObject.has(Tags.name))
                                    attractionModel.name = JSONParser.getString(jsonObject, Tags.name);

                                if (jsonObject.has(Tags.place_id))
                                    attractionModel.place_id = JSONParser.getString(jsonObject, Tags.place_id);

                                if (jsonObject.has(Tags.rating))
                                    attractionModel.rating = JSONParser.getString(jsonObject, Tags.rating);

                                if (jsonObject.has(Tags.lat))
                                    attractionModel.lat = JSONParser.getString(jsonObject, Tags.lat);

                                if (jsonObject.has(Tags.lng))
                                    attractionModel.lng = JSONParser.getString(jsonObject, Tags.lng);

//                                if(jsonObject.has(Tags.formatted_address))
//                                    attractionModel.formatted_address=JSONParser.getString(jsonObject, Tags.formatted_address);
//
//                                if(jsonObject.has(Tags.formatted_address))
//                                    attractionModel.formatted_address=JSONParser.getString(jsonObject, Tags.formatted_address);
//
//                                if(jsonObject.has(Tags.formatted_address))
//                                    attractionModel.formatted_address=JSONParser.getString(jsonObject, Tags.formatted_address);
//
//                                if(jsonObject.has(Tags.formatted_address))
//                                    attractionModel.formatted_address=JSONParser.getString(jsonObject, Tags.formatted_address);
                                attractionModelArrayList.add(attractionModel);

                            }
                            mAdapter.notifyDataSetChanged();
                            mAdapter.setLoaded();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

//    public void setAttractionListAdapter(/*ArrayList<WaitingJobsModel> waitingJobsModelArrayList*/) {
//        attractionModelArrayList = new ArrayList<>();
////        txt_c_title.setText("WAITING JOBS("+waitingJobsModelArrayList.size() + ")");
//        mAdapter = new AttractionAdapter(getActivity(), attractionModelArrayList);
//        recycler_view.setAdapter(mAdapter);
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
