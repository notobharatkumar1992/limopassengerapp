package com.limocar.passenger.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.limocar.passenger.R;
import com.limocar.passenger.adapters.AttractionAdapter;
import com.limocar.passenger.adapters.FavoriteAdapter;
import com.limocar.passenger.models.AttractionModel;
import com.limocar.passenger.models.FavoriteModel;

import java.util.ArrayList;


public class FavoriteListFragment extends Fragment {
    RecyclerView recycler_view;
    ArrayList<FavoriteModel> favoriteModelArrayList;
    private FavoriteAdapter mAdapter;

    public FavoriteListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_view.setHasFixedSize(true);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        setFavoriteListAdapter();
    }

    public void setFavoriteListAdapter(/*ArrayList<WaitingJobsModel> waitingJobsModelArrayList*/) {
        favoriteModelArrayList = new ArrayList<>();
//        txt_c_title.setText("WAITING JOBS("+waitingJobsModelArrayList.size() + ")");
        mAdapter = new FavoriteAdapter(getActivity(), favoriteModelArrayList);
        recycler_view.setAdapter(mAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
