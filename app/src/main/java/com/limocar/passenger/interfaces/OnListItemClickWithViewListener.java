package com.limocar.passenger.interfaces;

import android.view.View;

/**
 * Created by bharat on 26/12/15.
 */
public interface OnListItemClickWithViewListener {

    public void setOnListItemClickListener(String name, int position, View view);
}
