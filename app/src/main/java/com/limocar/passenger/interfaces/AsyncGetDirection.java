package com.limocar.passenger.interfaces;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public interface AsyncGetDirection {

	void OnDirectionCompleted(ArrayList<LatLng> listLatLng, String state);

}
