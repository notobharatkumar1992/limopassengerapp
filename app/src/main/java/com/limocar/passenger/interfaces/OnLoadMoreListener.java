package com.limocar.passenger.interfaces;

/**
 * Created by Heena on 03/09/2017.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
