package com.limocar.passenger.interfaces;

import android.content.Intent;

/**
 * Created by parul on 26/12/15.
 */
public interface FbLogin_Interface {

    public void fbLogin_Result(int request_code, int result_code, Intent data);
}
