package com.limocar.passenger.interfaces;

/**
 * Created by bharat on 26/12/15.
 */
public interface OnRangebarChangeListener {

    public void setOnRangebarChangeListener(String name, int position, String startRange, String endRange);
}
