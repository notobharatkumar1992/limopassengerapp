package com.limocar.passenger.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.limocar.passenger.AppDelegate;


public class MyClickableSpan extends ClickableSpan {

    private String keyword;
    private Context mContext;
    @SuppressWarnings("unused")
    private FragmentManager fm;

    public MyClickableSpan(Context context, String keyword, FragmentManager fm) {
        super();
        this.keyword = keyword;
        this.mContext = context;
        this.fm = fm;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setColor(ds.linkColor);
        ds.setUnderlineText(true);
    }

    @Override
    public void onClick(View v) {
        try {
            if (!keyword.contains("http")) {
                keyword = "http://" + keyword;
            }
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(keyword)));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        v.invalidate();
    }
}
