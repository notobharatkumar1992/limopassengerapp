package com.limocar.passenger.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 14-Feb-17.
 */

public class BookOrderModel implements Parcelable {
    public static final int PICKUP_NOW = 0, PICKUP_FUTURE = 1;
    public int pickup_type = PICKUP_NOW;
    public double picup_latitude = 0, dropoff_latitude = 0, pickup_longitude = 0, dropoff_longitude = 0;
    public String triptype, timetype, promotype, paymenttype, pickup_date, pickup_time, pickup_time_stamp, pickup_address, dropoff_address, serviceCategoryCode;

    public BookOrderModel() {
        pickup_time_stamp = (System.currentTimeMillis() / 1000) + "";
    }

    @Override
    public String toString() {
        return "BookOrderModel{" +
                "pickup_type=" + pickup_type +
                ", picup_latitude=" + picup_latitude +
                ", dropoff_latitude=" + dropoff_latitude +
                ", pickup_longitude=" + pickup_longitude +
                ", dropoff_longitude=" + dropoff_longitude +
                ", triptype='" + triptype + '\'' +
                ", timetype='" + timetype + '\'' +
                ", promotype='" + promotype + '\'' +
                ", paymenttype='" + paymenttype + '\'' +
                ", pickup_date='" + pickup_date + '\'' +
                ", pickup_time='" + pickup_time + '\'' +
                ", pickup_time_stamp='" + pickup_time_stamp + '\'' +
                ", pickup_address='" + pickup_address + '\'' +
                ", dropoff_address='" + dropoff_address + '\'' +
                ", serviceCategoryCode='" + serviceCategoryCode + '\'' +
                '}';
    }

    protected BookOrderModel(Parcel in) {
        pickup_type = in.readInt();
        picup_latitude = in.readDouble();
        dropoff_latitude = in.readDouble();
        pickup_longitude = in.readDouble();
        dropoff_longitude = in.readDouble();
        triptype = in.readString();
        timetype = in.readString();
        promotype = in.readString();
        paymenttype = in.readString();
        pickup_date = in.readString();
        pickup_time = in.readString();
        pickup_time_stamp = in.readString();
        pickup_address = in.readString();
        dropoff_address = in.readString();
        serviceCategoryCode = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(pickup_type);
        dest.writeDouble(picup_latitude);
        dest.writeDouble(dropoff_latitude);
        dest.writeDouble(pickup_longitude);
        dest.writeDouble(dropoff_longitude);
        dest.writeString(triptype);
        dest.writeString(timetype);
        dest.writeString(promotype);
        dest.writeString(paymenttype);
        dest.writeString(pickup_date);
        dest.writeString(pickup_time);
        dest.writeString(pickup_time_stamp);
        dest.writeString(pickup_address);
        dest.writeString(dropoff_address);
        dest.writeString(serviceCategoryCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookOrderModel> CREATOR = new Creator<BookOrderModel>() {
        @Override
        public BookOrderModel createFromParcel(Parcel in) {
            return new BookOrderModel(in);
        }

        @Override
        public BookOrderModel[] newArray(int size) {
            return new BookOrderModel[size];
        }
    };
}
