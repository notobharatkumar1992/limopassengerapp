package com.limocar.passenger.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 08-Feb-17.
 */

public class HotelsModel implements Parcelable {
    public String cat, display_name, display_content, icon;
    public boolean isSelected = false;

    public HotelsModel() {
    }

    @Override
    public String toString() {
        return "TaxiTypeModel{" +
                "cat='" + cat + '\'' +
                ", display_name='" + display_name + '\'' +
                ", display_content='" + display_content + '\'' +
                ", icon='" + icon + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }

    protected HotelsModel(Parcel in) {
        cat = in.readString();
        display_name = in.readString();
        display_content = in.readString();
        icon = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cat);
        dest.writeString(display_name);
        dest.writeString(display_content);
        dest.writeString(icon);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HotelsModel> CREATOR = new Creator<HotelsModel>() {
        @Override
        public HotelsModel createFromParcel(Parcel in) {
            return new HotelsModel(in);
        }

        @Override
        public HotelsModel[] newArray(int size) {
            return new HotelsModel[size];
        }
    };
}
