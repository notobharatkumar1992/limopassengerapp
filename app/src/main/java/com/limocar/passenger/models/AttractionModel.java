package com.limocar.passenger.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 08-Feb-17.
 */

public class AttractionModel implements Parcelable {
    public String cat, display_name, display_content, icon;
    public boolean isSelected = false;
    public String next_page_token, count, formatted_address, name, place_id, lat, lng, rating, exceptional_date, weekday_text, price_level;
    public boolean open_now;

    public AttractionModel() {
    }

    @Override
    public String toString() {
        return "AttractionModel{" +
                "display_content='" + display_content + '\'' +
                ", cat='" + cat + '\'' +
                ", display_name='" + display_name + '\'' +
                ", icon='" + icon + '\'' +
                ", isSelected=" + isSelected +
                ", next_page_token='" + next_page_token + '\'' +
                ", count='" + count + '\'' +
                ", formatted_address='" + formatted_address + '\'' +
                ", name='" + name + '\'' +
                ", place_id='" + place_id + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", rating='" + rating + '\'' +
                ", exceptional_date='" + exceptional_date + '\'' +
                ", weekday_text='" + weekday_text + '\'' +
                ", price_level='" + price_level + '\'' +
                ", open_now=" + open_now +
                '}';
    }

    protected AttractionModel(Parcel in) {
        cat = in.readString();
        display_name = in.readString();
        display_content = in.readString();
        icon = in.readString();
        isSelected = in.readByte() != 0;
        next_page_token = in.readString();
        count = in.readString();
        formatted_address = in.readString();
        name = in.readString();
        place_id = in.readString();
        lat = in.readString();
        lng = in.readString();
        rating = in.readString();
        exceptional_date = in.readString();
        weekday_text = in.readString();
        price_level = in.readString();
        open_now = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cat);
        dest.writeString(display_name);
        dest.writeString(display_content);
        dest.writeString(icon);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeString(next_page_token);
        dest.writeString(count);
        dest.writeString(formatted_address);
        dest.writeString(name);
        dest.writeString(place_id);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeString(rating);
        dest.writeString(exceptional_date);
        dest.writeString(weekday_text);
        dest.writeString(price_level);
        dest.writeByte((byte) (open_now ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AttractionModel> CREATOR = new Creator<AttractionModel>() {
        @Override
        public AttractionModel createFromParcel(Parcel in) {
            return new AttractionModel(in);
        }

        @Override
        public AttractionModel[] newArray(int size) {
            return new AttractionModel[size];
        }
    };
}
