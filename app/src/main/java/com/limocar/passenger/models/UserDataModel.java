package com.limocar.passenger.models;

import android.os.Parcel;
import android.os.Parcelable;


public class UserDataModel implements Parcelable {
    public int id, postal_code, category_id, sub_category_id, membership_id, login_type, device_type, is_verified, is_login, is_view, beer_id, brewery_id;
    public String userId, name, first_name, last_name, email, phone, address, country_id, state_id, city_id, dob, k2, image, session, k1, verification_code, device_id, created, password, file_path;
    public String province, city;

    public UserDataModel() {
    }

    @Override
    public String toString() {
        return "UserDataModel{" +
                "id=" + id +
                ", postal_code=" + postal_code +
                ", category_id=" + category_id +
                ", sub_category_id=" + sub_category_id +
                ", membership_id=" + membership_id +
                ", login_type=" + login_type +
                ", device_type=" + device_type +
                ", is_verified=" + is_verified +
                ", is_login=" + is_login +
                ", is_view=" + is_view +
                ", beer_id=" + beer_id +
                ", brewery_id=" + brewery_id +
                ", userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", country_id='" + country_id + '\'' +
                ", state_id='" + state_id + '\'' +
                ", city_id='" + city_id + '\'' +
                ", dob='" + dob + '\'' +
                ", k2='" + k2 + '\'' +
                ", image='" + image + '\'' +
                ", session='" + session + '\'' +
                ", k1='" + k1 + '\'' +
                ", verification_code='" + verification_code + '\'' +
                ", device_id='" + device_id + '\'' +
                ", created='" + created + '\'' +
                ", password='" + password + '\'' +
                ", file_path='" + file_path + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    protected UserDataModel(Parcel in) {
        id = in.readInt();
        postal_code = in.readInt();
        category_id = in.readInt();
        sub_category_id = in.readInt();
        membership_id = in.readInt();
        login_type = in.readInt();
        device_type = in.readInt();
        is_verified = in.readInt();
        is_login = in.readInt();
        is_view = in.readInt();
        beer_id = in.readInt();
        brewery_id = in.readInt();
        userId = in.readString();
        name = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        phone = in.readString();
        address = in.readString();
        country_id = in.readString();
        state_id = in.readString();
        city_id = in.readString();
        dob = in.readString();
        k2 = in.readString();
        image = in.readString();
        session = in.readString();
        k1 = in.readString();
        verification_code = in.readString();
        device_id = in.readString();
        created = in.readString();
        password = in.readString();
        file_path = in.readString();
        province = in.readString();
        city = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(postal_code);
        dest.writeInt(category_id);
        dest.writeInt(sub_category_id);
        dest.writeInt(membership_id);
        dest.writeInt(login_type);
        dest.writeInt(device_type);
        dest.writeInt(is_verified);
        dest.writeInt(is_login);
        dest.writeInt(is_view);
        dest.writeInt(beer_id);
        dest.writeInt(brewery_id);
        dest.writeString(userId);
        dest.writeString(name);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(address);
        dest.writeString(country_id);
        dest.writeString(state_id);
        dest.writeString(city_id);
        dest.writeString(dob);
        dest.writeString(k2);
        dest.writeString(image);
        dest.writeString(session);
        dest.writeString(k1);
        dest.writeString(verification_code);
        dest.writeString(device_id);
        dest.writeString(created);
        dest.writeString(password);
        dest.writeString(file_path);
        dest.writeString(province);
        dest.writeString(city);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserDataModel> CREATOR = new Creator<UserDataModel>() {
        @Override
        public UserDataModel createFromParcel(Parcel in) {
            return new UserDataModel(in);
        }

        @Override
        public UserDataModel[] newArray(int size) {
            return new UserDataModel[size];
        }
    };
}
