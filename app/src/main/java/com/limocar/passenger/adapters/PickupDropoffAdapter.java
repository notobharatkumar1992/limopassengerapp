package com.limocar.passenger.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.constants.Tags;
import com.limocar.passenger.interfaces.OnListItemClickListener;
import com.limocar.passenger.models.Place;

import java.util.List;

import carbon.widget.LinearLayout;

public class PickupDropoffAdapter extends BaseAdapter {

    private Context mContext;
    public List<Place> placesArray;
    public OnListItemClickListener clickListener;
    int PICK_UP;
    public PickupDropoffAdapter(Context mContext, List<Place> placesArray, OnListItemClickListener clickListener) {
        this.mContext = mContext;
        this.placesArray = placesArray;
        this.clickListener = clickListener;
    }
    public PickupDropoffAdapter(Context mContext, List<Place> placesArray, OnListItemClickListener clickListener, int PICK_UP) {
        this.mContext = mContext;
        this.placesArray = placesArray;
        this.clickListener = clickListener;
        this.PICK_UP=PICK_UP;
    }
    @Override
    public int getCount() {
        return (placesArray != null ? (placesArray.size() > 5 ? 5 : placesArray.size()) : 0);
    }

    @Override
    public Object getItem(int position) {
        return placesArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.map_list_item, null);

            holder = new Holder();
            holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);
            holder.ll_c_main_item = (LinearLayout) convertView.findViewById(R.id.ll_c_main_item);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.txt_name.setTag(position);

        holder.txt_name.setText("");
        if (placesArray != null && placesArray.size() > 0 && position < placesArray.size()) {
            Place place = placesArray.get(position);
            AppDelegate.LogT("adapter => " + place.getName() + ", " + place.getVicinity());
            if (AppDelegate.isValidString(place.getName()) && AppDelegate.isValidString(place.getVicinity())) {
                holder.txt_name.setText(place.getName() + ", " + place.getVicinity());
            } else if (AppDelegate.isValidString(place.getName())) {
                holder.txt_name.setText(place.getName());
            } else if (AppDelegate.isValidString(place.getVicinity())) {
                holder.txt_name.setText(place.getVicinity());
            }

            holder.ll_c_main_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null)
                        clickListener.setOnListItemClickListener(Tags.ADDRESS, position,PICK_UP);
                }
            });
        } else {
            AppDelegate
                    .LogE("LocationMapAdapter length placesArray is null or 0 = "
                            + position);
        }
        return convertView;
    }

    class Holder {
        public TextView txt_name;
        public LinearLayout ll_c_main_item;
    }

}
