package com.limocar.passenger.adapters;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.models.FavoriteModel;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;


public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {
    public int position;
    private final ArrayList<FavoriteModel> favoriteModelArrayList;
    View v;
    FragmentActivity context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        FavoriteModel favoriteModel = favoriteModelArrayList.get(position);
        holder.img_c_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AppDelegate.showFragmentAnimation(context.getSupportFragmentManager(), new HomeFragment(), R.id.main_containt);
            }
        });
    }

    public FavoriteAdapter(FragmentActivity context, ArrayList<FavoriteModel> newsModelArrayList) {
        this.context = context;
        this.favoriteModelArrayList = newsModelArrayList;
    }

    @Override
    public int getItemCount() {
        return 15;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_name, txt_c_detail;
        ImageView img_c_favorite;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_name = (TextView) itemView.findViewById(R.id.txt_c_name);
            txt_c_detail = (TextView) itemView.findViewById(R.id.txt_c_detail);
            img_c_favorite = (ImageView) itemView.findViewById(R.id.img_c_favorite);
        }
    }
}
