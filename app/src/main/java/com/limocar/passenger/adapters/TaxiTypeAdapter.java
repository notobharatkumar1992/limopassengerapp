package com.limocar.passenger.adapters;


import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.limocar.passenger.AppDelegate;
import com.limocar.passenger.R;
import com.limocar.passenger.interfaces.OnListItemClickListener;
import com.limocar.passenger.models.TaxiTypeModel;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;


public class TaxiTypeAdapter extends RecyclerView.Adapter<TaxiTypeAdapter.ViewHolder> {
    public int position;
    private final ArrayList<TaxiTypeModel> waitingJobsModelArrayList;
    View v;
    FragmentActivity context;
    OnListItemClickListener itemClickListener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.taxitype_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TaxiTypeModel taxiTypeModel = waitingJobsModelArrayList.get(position);
        holder.txt_c_taxitype.setText(taxiTypeModel.display_name);
        AppDelegate.LogT("taxi type==>" + taxiTypeModel.display_name);
        try {
            String[] safe0 = taxiTypeModel.icon.split(",");
//            String[] safe = safe0[1].split("=");
            byte[] imageAsBytes = Base64.decode(safe0[1].getBytes(), Base64.DEFAULT);
            holder.img_taxi.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (waitingJobsModelArrayList.get(position).isSelected) {
            holder.ll_c_main.setBackgroundColor(context.getResources().getColor(R.color.lightgray));
            holder.img_taxi.setBackgroundColor(context.getResources().getColor(R.color.lightgray));
            holder.txt_c_taxitype.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.ll_c_main.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.img_taxi.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.txt_c_taxitype.setTextColor(context.getResources().getColor(R.color.et_hint));
        }
        holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null) {
                    itemClickListener.setOnListItemClickListener("state", position);
                }
            }
        });
    }

    public TaxiTypeAdapter(FragmentActivity context, ArrayList<TaxiTypeModel> newsModelArrayList, OnListItemClickListener itemClickListener) {
        this.context = context;
        this.waitingJobsModelArrayList = newsModelArrayList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return waitingJobsModelArrayList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_taxitype;
        ImageView img_taxi;
        LinearLayout ll_c_main;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_taxitype = (TextView) itemView.findViewById(R.id.txt_c_taxitype);
            img_taxi = (ImageView) itemView.findViewById(R.id.img_taxi);
            ll_c_main = (LinearLayout) itemView.findViewById(R.id.ll_c_main);

        }
    }
}
