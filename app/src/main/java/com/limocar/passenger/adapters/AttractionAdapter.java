package com.limocar.passenger.adapters;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.limocar.passenger.R;
import com.limocar.passenger.fragments.AttractionListFragment;
import com.limocar.passenger.interfaces.OnLoadMoreListener;
import com.limocar.passenger.models.AttractionModel;
import com.limocar.passenger.models.FavoriteModel;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;


public class AttractionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public int position;
    private final ArrayList<AttractionModel> favoriteModelArrayList;
    View v;
    FragmentActivity context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener mOnLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.fragment_list_item, parent, false);
            return new ViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            AttractionModel user = favoriteModelArrayList.get(position);
            ViewHolder userViewHolder = (ViewHolder) holder;
            userViewHolder.txt_c_name.setText(user.name);
            userViewHolder.txt_c_detail.setText(user.formatted_address);
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

//        holder.img_c_favorite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                AppDelegate.showFragmentAnimation(context.getSupportFragmentManager(), new HomeFragment(), R.id.main_containt);
//            }
//        });
    }


//    @Override
//    public void onBindViewHolder(final RecyclerView holder, final int position) {
////        AttractionModel favoriteModel = favoriteModelArrayList.get(position);
//
//    }

    public AttractionAdapter(FragmentActivity context, ArrayList<AttractionModel> newsModelArrayList, RecyclerView mRecyclerView) {
        this.context = context;
        this.favoriteModelArrayList = newsModelArrayList;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return favoriteModelArrayList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return favoriteModelArrayList == null ? 0 : favoriteModelArrayList.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_name, txt_c_detail;
        ImageView img_c_favorite;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_name = (TextView) itemView.findViewById(R.id.txt_c_name);
            txt_c_detail = (TextView) itemView.findViewById(R.id.txt_c_detail);
            img_c_favorite = (ImageView) itemView.findViewById(R.id.img_c_favorite);
        }
    }
}
