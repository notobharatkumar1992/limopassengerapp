package com.limocar.passenger.net;

import com.limocar.passenger.AppDelegate;

import retrofit.RetrofitError;

public abstract class Callback<T> implements retrofit.Callback<T> {

    public abstract void failure(RestError restError);

    @Override
    public void failure(RetrofitError error) {
        try {
//            s
            AppDelegate.LogT("failure called => " + error.getMessage() + ", " + error.getResponse().toString());
            RestError restError = (RestError) error.getBodyAs(RestError.class);
            if (restError != null)
                failure(restError);
            else {
                failure(new RestError(error.getMessage()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}