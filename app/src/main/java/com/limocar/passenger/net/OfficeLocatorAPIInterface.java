package com.limocar.passenger.net;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface OfficeLocatorAPIInterface {


    @FormUrlEncoded
    @POST("/passenger/login")
    void userLogin(@Field("u") String username,
                   @Field("p") String password,
                   @Field("uuid") String device_uuid,
                   /*@Field("device_type") String device_type,*/
                   @Field("notify_token") String notify_token,
                   Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/passenger/register")
    void userRegistration(@Field("u") String username,
                          @Field("p") String password,
                          @Field("uuid") String device_uuid,
                          @Field("notify_token") String notify_token,
                          @Field("first_name") String first_name,
                          @Field("last_name") String last_name,
                          @Field("phone") String phone,
                          @Field("pic") String pic,
                          Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/passenger/reset_password")
    void userForgetPassword(@Field("u") String username,
                            Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/passenger/profile/update")
    void updateProfile(@Field("u") String username,
                       @Field("first_name") String first_name,
                       @Field("last_name") String last_name,
                       @Field("phone") String phone,
                       @Field("pic") String pic,
                       Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/passenger/logout")
    void logout(@Field("session") String session, Callback<Response> responseCallback);

    @GET("/passenger/services")
    void getTaxiType(Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/order")
    void bookOrder(@Field("cat") String serviceCategoryCode,
                   @Field("pu_lat") String pickUp_latitude,
                   @Field("pu_lng") String pickUp_longitude,
                   @Field("do_lat") String dropOff_latitude,
                   @Field("do_lng") String dropOff_longitude,
                   @Field("ride_type") String ride_type,
                   @Field("ride_time") String ride_time,
                   @Field("payment_type") String payment_type,
                   @Field("promo_code") String promo_code,
                   @Field("driver_note") String driver_note,
                   @Field("pu_place_id") String pu_place_id,
                   @Field("do_place_id") String do_place_id,
                   @Field("psg_first_name") String psg_first_name,
                   @Field("psg_last_name") String psg_last_name,
                   @Field("psg_contact") String psg_contact,
                   @Field("psg_email") String psg_email,
                   Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/order/quote")
    void getTheQuoteForTrip(@Field("cat") String serviceCategoryCode,
                            @Field("pu_lat") String pickUp_latitude,
                            @Field("pu_lng") String pickUp_longitude,
                            @Field("do_lat") String dropOff_latitude,
                            @Field("do_lng") String dropOff_longitude,
                            @Field("ride_type") String ride_type,
                            @Field("ride_time") String ride_time,
//                            @Field("payment_type") String payment_type,
//                            @Field("promo_code") String promo_code,
//                            @Field("driver_note") String driver_note,
                            Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/passenger/settings")
    void sendSettings(@Field("user_id") String user_id,
                      @Field("push_notification") String push_notification,
                      @Field("sound") String sound,
                      @Field("vibration") String vibration,
                      @Field("lang") String lang,
                      @Field("latitude") String latitude,
                      @Field("longitude") String longitude,
                      @Field("radius") String radius,
                      Callback<Response> responseCallback);

    @GET("/location/attractions")
    void getAttractions(@Query("next_page_token") String next_page_token,
                        Callback<Response> responseCallback);

    @GET("/location/hotels")
    void getHotels(@Query("next_page_token") String next_page_token,
                   Callback<Response> responseCallback);

    @GET("/location/transports")
    void getTransport(@Query("next_page_token") String next_page_token,
                      Callback<Response> responseCallback);

    //    @GET("/location/attractions")
//    void getAttractions(@Query("next_page_token") String next_page_token,
//                        Callback<Response> responseCallback);
    @GET("/passenger/getSettings")
    void getSettingDetails(@Query("user_id") String user_id,
                           Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/passenger/userProfile")
    void getUserProfile(@Field("user_id") String user_id,
                        Callback<Response> responseCallback);


}

