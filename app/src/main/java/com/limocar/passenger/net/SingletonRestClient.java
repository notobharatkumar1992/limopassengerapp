package com.limocar.passenger.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.limocar.passenger.AppDelegate;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by Dev on 9/15/2015.
 */
public class SingletonRestClient {
    private static OfficeLocatorAPIInterface REST_CLIENT;
    private static final String API_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";

    private static String ROOT = "http://limoapi.zenbitservers.com/";
    private static RestAdapter restadapter;
    //    private static String ROOT = "http://192.168.100.81/newapp/user_webservices/";

    static {
        setupRestClient();
    }

    private SingletonRestClient() {
    }

    public static OfficeLocatorAPIInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        Gson gson = new GsonBuilder()
                .setDateFormat(API_DATE_FORMAT)
                .serializeNulls()
                .create();

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Content-Type", "application/x-www-form-urlencoded");
                request.addHeader("X-API-KEY", "s4ko08cocg8kocwc4co4kkckws0ssoo0okk888sc");
                if (AppDelegate.isValidString(AppDelegate.session)) {
                    request.addHeader("session", AppDelegate.session);
                }
            }
        };
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(ROOT)
                .setClient(new OkClient(getClient()))
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL);
        RestAdapter restAdapter = builder.setRequestInterceptor(requestInterceptor).build();
        REST_CLIENT = restAdapter.create(OfficeLocatorAPIInterface.class);
    }
    private static OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(5, TimeUnit.MINUTES);
        client.setReadTimeout(5, TimeUnit.MINUTES);
        return client;
    }
}
