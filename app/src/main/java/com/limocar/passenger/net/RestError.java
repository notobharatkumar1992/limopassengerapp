package com.limocar.passenger.net;

import com.google.gson.annotations.SerializedName;

public class RestError {

    public RestError() {
    }

    @SerializedName("code")
    private Integer code;

    @SerializedName("error_message")
    private String strMessage;

    public RestError(String strMessage) {
        this.strMessage = strMessage;
    }

    //Getters and setters

    /**
     * @return The code
     */
    public Integer getcode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setcode(Integer code) {
        this.code = code;
    }

    /**
     * @return The strMessage
     */
    public String getstrMessage() {
        return strMessage;
    }

    /**
     * @param strMessage The strMessage
     */
    public void setstrMessage(String strMessage) {
        this.strMessage = strMessage;
    }
}