package com.limocar.passenger.firebase;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Heena on 5/27/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFireBaseMsgService";

    public static final int PURCHASED_DEAL = 1, NEWLY_ADDED_HERO_DEALS = 2, NEWLY_ADDED_ADHOC_DEALS = 3, REDEEM_PURCHASE_ADHOC_DEAL = 4, REDEEM_HERO_DEAL = 5,
            UPGRADED_MEMBERSHIP = 6, MEMBERSHIP_EXPIRING = 7, MEMBERSHIP_EXPIRED = 8, EXPIRING_PURCHASE_DEAL = 9, EXPIRED_PURCHASE = 10;

    /*
    Notification Type
    1 = Purchase Deal
    2 = Newly added Hero deals
    3 = Newly added Adhoc deals
    4 = Redeem purchase Adhoc deal
    5 = Redeem purchase Hero deal
    6 = Upgraded membership
    7 = Membership expiring
    8 = Membership Expired
    9 = Expiring purchase deal
    10 = Expired purchase
    */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
        Log.d(TAG, "remoteMessage: " + remoteMessage.getData());
        Log.d(TAG, "From: " + remoteMessage.getFrom());
    }

}